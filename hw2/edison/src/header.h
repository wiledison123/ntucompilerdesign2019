#ifndef _H_SYMBOLTABLE_
#define _H_SYMBOLTABLE_

struct symtab{
	char lexeme[32];
	struct symtab *front;
	struct symtab *back;
	int line;
	int counter;
};

typedef struct symtab symtab;
symtab * lookup(char *name);
void insertID(char *name);
void printSymTab();
void fillSymTab(char ***name, int **cnt, int *n);

#endif // _H_SYMBOLTABLE_
