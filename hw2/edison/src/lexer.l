%option noyywrap 
%{
#include <assert.h>
#include <stdio.h>
#include "header.h"
int linenumber;
void AddIdentifier(char *name);
void AddComment(char *comment);
void AddToken(char *TOKEN);

#define RETURN 0
#define TYPEDEF 1
#define IF 2
#define ELSE 3
#define INT 4
#define FLOAT 5
#define FOR 6
#define VOID 7
#define WHILE 8
#define INT_LIT 10
#define FLOAT_LIT 11
#define STRING_LIT 12
#define IDENT 20
#define OP_ADD 30
#define OP_SUB 31
#define OP_MUL 32
#define OP_DIV 33
#define OP_LT 34
#define OP_GT 35
#define OP_LE 36
#define OP_GE 37
#define OP_NEQ 38
#define OP_EQ 39
#define OP_OR 40
#define OP_AND 41
#define OP_NOT 42
#define OP_ASSIGN 43
#define LBIG 50
#define RBIG 51
#define LMID 52
#define RMID 53
#define LSML 54
#define RSML 55
#define SEMICOL 56
#define COMMA 57
#define DOT 58
#define COMMENT 98
#define WSPACE 99
#define ERROR 100

%}

RETURN          "return"
TYPEDEF         "typedef"
IF              "if"
ELSE            "else"
INT             "int"
FLOAT           "float"
FOR             "for"
VOID            "void"
WHILE           "while"

newline         "\n"
letter          [A-Za-z]
digit           [0-9]
IDENT           {letter}({letter}|{digit}|"_")*
WSPACE          [ \t]+
INT_LIT         {digit}+
FLOAT_LIT       (((({digit}+"."{digit}*)|({digit}*"."{digit}+))([eE][+-]?{digit}+)?)|(({digit}+)[eE][+-]?{digit}+))[fFlL]?
STRING_LIT      "\""[^"]*"\""
COMMENT         "/*"(([^*]|{newline})*|"*"+[^*/]+)*"*"*"*/"

OP_ADD          "+"
OP_SUB          "-"
OP_MUL          "*"
OP_DIV          "/"
OP_LT           "<"
OP_GT           ">"
OP_LE           "<="
OP_GE           ">="
OP_NEQ          "!="
OP_EQ           "=="
OP_OR           "||"
OP_AND          "&&"
OP_NOT          "!"
OP_ASSIGN       "="

LBIG            "{"
RBIG            "}"
LMID            "["
RMID            "]"
LSML            "("
RSML            ")"
SEMICOL         ";"
COMMA           ","
DOT             "."

error    .

%%
{RETURN}            { AddToken("RETURN"); }
{TYPEDEF}           { AddToken("TYPEDEF"); }
{IF}                { AddToken("IF"); }
{ELSE}              { AddToken("ELSE"); }
{INT}               { AddToken("INT"); }
{FLOAT}             { AddToken("FLOAT"); }
{FOR}               { AddToken("FOR"); }
{VOID}              { AddToken("VOID"); }
{WHILE}             { AddToken("WHILE"); }

{IDENT}             { AddIdentifier(yytext); AddToken("IDENT"); }
{WSPACE}            { AddToken("WSPACE"); }
{INT_LIT}           { AddToken("INT_LIT"); }
{FLOAT_LIT}         { AddToken("FLOAT_LIT"); }
{STRING_LIT}        { AddToken("STRING_LIT"); }
{COMMENT}           { AddComment(yytext); AddToken("COMMENT"); }

{OP_ADD}            { AddToken("OP_ADD"); }
{OP_SUB}            { AddToken("OP_SUB"); }
{OP_MUL}            { AddToken("OP_MUL"); }
{OP_DIV}            { AddToken("OP_DIV"); }
{OP_LT}             { AddToken("OP_LT"); }
{OP_GT}             { AddToken("OP_GT"); }
{OP_LE}             { AddToken("OP_LE"); }
{OP_GE}             { AddToken("OP_GE"); }
{OP_NEQ}            { AddToken("OP_NEQ"); }
{OP_EQ}             { AddToken("OP_EQ"); }
{OP_OR}             { AddToken("OP_OR"); }
{OP_AND}            { AddToken("OP_AND"); }
{OP_NOT}            { AddToken("OP_NOT"); }
{OP_ASSIGN}         { AddToken("OP_ASSIGN"); }

{newline}           { linenumber += 1; }

{LBIG}              { AddToken("LBIG"); }
{RBIG}              { AddToken("RBIG"); }
{LMID}              { AddToken("LBIG"); }
{RMID}              { AddToken("RMID"); }
{LSML}              { AddToken("LSML"); }
{RSML}              { AddToken("RSML"); }
{SEMICOL}           { AddToken("SEMICOL"); }
{COMMA}             { AddToken("COMMA"); }
{DOT}               { AddToken("DOT"); }

{error}             printf("ERR \n");


%%

void AddIdentifier(char *name) {
    symtab *ptr = lookup(name);
    if (ptr == NULL) insertID(name);
    else ptr->counter++;
}

void AddComment(char *comment) {
    printf("%s\n", comment);
}

void AddToken(char *TOKEN) {
    // printf("Get token id = %s\n", TOKEN);
}

int main(int argc, char **argv) {
    if (argc > 1) yyin = fopen(argv[1], "r");
    else yyin = stdin;

    yylex();

    char **name; int *cnt, n;
    fillSymTab(&name, &cnt, &n);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n - 1; ++j) {
            if (strcmp(name[i], name[j]) < 0) {
                char *tmp = name[i]; name[i] = name[j]; name[j] = tmp;
                int ttmp = cnt[i]; cnt[i] = cnt[j]; cnt[j] = ttmp;
            }
        }
    }

    // printf("n = %d\n", n);
    printf("\nFrequency of identifiers:\n");
    for (int i = 0; i < n; ++i) {
        printf("%s\t%d\n", name[i], cnt[i]);
    }
}
