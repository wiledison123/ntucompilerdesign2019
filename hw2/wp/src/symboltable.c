#include <ctype.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"

#define TABLE_SIZE 512

symtab *hash_table[TABLE_SIZE];
extern int line_number;

int hash(char* str) {
	uint32_t idx = 0;
	while (*str) {
		idx = idx << 1;
		idx += *str;
		str++;
	}
	return (idx & (TABLE_SIZE - 1));
}

/* returns the symbol table entry if found else NULL */

symtab *lookup(char *name) {
	int hash_key;
	symtab *symptr;
	if (!name) return NULL;
	hash_key = hash(name);
	symptr = hash_table[hash_key];

	while (symptr) {
		if (!(strcmp(name, symptr->lexeme))) return symptr;
		symptr = symptr->front;
	}
	return NULL;
}

void insertId(char *name) {
	int hash_key;
	symtab *ptr;
	symtab *symptr = (symtab*)malloc(sizeof(symtab));

	hash_key = hash(name);
	ptr = hash_table[hash_key];

	if (ptr == NULL) {
		/* first entry for this hash_key */
		hash_table[hash_key] = symptr;
		symptr->front = NULL;
		symptr->back = symptr;
	} else {
		symptr->front = ptr;
		ptr->back = symptr;
		symptr->back = symptr;
		hash_table[hash_key] = symptr;
	}

	strcpy(symptr->lexeme, name);
	symptr->line = line_number;
	symptr->counter = 1;
}

void printSymTab() {
	int i, j;
	int entry_count = 0, cur_entry = 0;
	symtab **identifiers;
	printf("\nFrequency of identifiers:\n");
	for (i = 0; i < TABLE_SIZE; i++) {
		symtab *symptr;
		symptr = hash_table[i];
		while (symptr != NULL) {
			symptr = symptr->front;
			++entry_count;
		}
	}
	identifiers = (symtab**)malloc(sizeof(symtab*) * entry_count);
	for (i = 0; i < TABLE_SIZE; i++) {
		symtab *symptr;
		symptr = hash_table[i];
		while (symptr != NULL) {
			identifiers[cur_entry++] = symptr;
			symptr = symptr->front;
		}
	}
	for (i = 0; i < entry_count; i++) {
		for (j = i + 1; j < entry_count; j++) {
			if (strcmp(identifiers[i]->lexeme, identifiers[j]->lexeme) > 0) {
				symtab *tmp = identifiers[i];
				identifiers[i] = identifiers[j];
				identifiers[j] = tmp;
			}
		}
	}
	for (i = 0; i < entry_count; i++) {
		printf("%s\t%d\n", identifiers[i]->lexeme, identifiers[i]->counter);
	}
}
