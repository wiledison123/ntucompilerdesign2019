%option noyywrap

%{
	#include <stdio.h>

	#include "header.h" 

	#ifdef DEBUG
	#define DO_DEBUG(str) { fprintf(stderr, "%s: %s\n", str, yytext); }
	#else
	#define DO_DEBUG(str) {}
	#endif

	int line_number;
	symtab *ptr;

	/* TODO: Define tokens? Currently unused. */
	/* #define INT 10
	   #define FLOAT 11 
	   #define OP_ASSIGN 12 
	   #define OP_OR 13 
	   #define MK_LPAREN 14 
	   #define MK_RPAREN 15 
	   #define ERROR 100 */
%}

%x COMMENTMODE

LETTER [A-Za-z]
DIGIT [0-9]
WS [ \t]+
NEWLINE "\n"

/* Reserved words */
RES_RETURN "return"
RES_TYPEDEF "typedef"
RES_IF "if"
RES_ELSE "else"
RES_INT "int"
RES_FLOAT "float"
RES_FOR "for"
RES_VOID "void"
RES_WHILE "while"

/* Constants */
INT_CONSTANT {DIGIT}+
FLOAT_EXPONENT (e|E)([+]|-)?{INT_CONSTANT}
FLOAT_DOTTED_DIGITS {INT_CONSTANT}[.]|[.]{INT_CONSTANT}|{INT_CONSTANT}[.]{INT_CONSTANT}
FLOAT_CONSTANT ({FLOAT_DOTTED_DIGITS}|{INT_CONSTANT}){FLOAT_EXPONENT}?[fFlL]?
STRING_CONSTANT ["][^"]*["]

/* Identifiers */
ID {LETTER}({LETTER}|{DIGIT}|"_")*

/* Symbols and operators */
OP_PLUS "+"
OP_MINUS "-"
OP_MULTIPLY "*"
OP_DIVIDE "/"

OP_LT "<"
OP_GT ">"
OP_LEQ "<="
OP_GEQ ">="
OP_NEQ "!="
OP_EQ "=="

OP_OR "||"
OP_AND "&&"
OP_NOT "!"

OP_ASSIGN "="

/* Separators */

DL_LBRACE "{"
DL_RBRACE "}"
DL_LBRACK "["
DL_RBRACK "]"
DL_LPAREN "("
DL_RPAREN ")"
DL_SEMICOL ";"
DL_COMMA ","
DL_DOT "."

ERROR .

%%

{WS} {}
{NEWLINE} line_number += 1;

"/*" { DO_DEBUG("BEGCOMMENT"); printf("%s", yytext); BEGIN(COMMENTMODE); }
<COMMENTMODE>[^*]* { printf("%s", yytext); }
<COMMENTMODE>[*]+[^*/]* { printf("%s", yytext); }
<COMMENTMODE>[*]+[/] { DO_DEBUG("ENDCOMMENT"); printf("%s\n", yytext); BEGIN(INITIAL); }

{RES_RETURN} DO_DEBUG("RES_RETURN")
{RES_TYPEDEF} DO_DEBUG("RES_TYPEDEF")
{RES_IF} DO_DEBUG("RES_IF")
{RES_ELSE} DO_DEBUG("RES_ELSE")
{RES_INT} DO_DEBUG("RES_INT")
{RES_FLOAT} DO_DEBUG("RES_FLOAT")
{RES_FOR} DO_DEBUG("RES_FOR")
{RES_VOID} DO_DEBUG("RES_VOID")
{RES_WHILE} DO_DEBUG("RES_WHILE")

{INT_CONSTANT} DO_DEBUG("INT_CONSTANT")
{FLOAT_CONSTANT} DO_DEBUG("FLOAT_CONSTANT")
{STRING_CONSTANT} DO_DEBUG("STRING_CONSTANT")

{ID} { 
	DO_DEBUG("ID");
	ptr = lookup(yytext);
	if (ptr == NULL)
		insertId(yytext);  
	else
		ptr->counter++;
}

{OP_PLUS} DO_DEBUG("OP_PLUS")
{OP_MINUS} DO_DEBUG("OP_MINUS")
{OP_MULTIPLY} DO_DEBUG("OP_MULTIPLY")
{OP_DIVIDE} DO_DEBUG("OP_DIVIDE")

{OP_LT} DO_DEBUG("OP_LT")
{OP_GT} DO_DEBUG("OP_GT")
{OP_LEQ} DO_DEBUG("OP_LEQ")
{OP_GEQ} DO_DEBUG("OP_GEQ")
{OP_NEQ} DO_DEBUG("OP_NEQ")
{OP_EQ} DO_DEBUG("OP_EQ")

{OP_OR} DO_DEBUG("OP_OR")
{OP_AND} DO_DEBUG("OP_AND")
{OP_NOT} DO_DEBUG("OP_NOT")

{OP_ASSIGN} DO_DEBUG("OP_ASSIGN")

{DL_LBRACE} DO_DEBUG("DL_LBRACE")
{DL_RBRACE} DO_DEBUG("DL_RBRACE")
{DL_LBRACK} DO_DEBUG("DL_LBRACK")
{DL_RBRACK} DO_DEBUG("DL_RBRACK")
{DL_LPAREN} DO_DEBUG("DL_LPAREN")
{DL_RPAREN} DO_DEBUG("DL_RPAREN")
{DL_SEMICOL} DO_DEBUG("DL_SEMICOL")
{DL_COMMA} DO_DEBUG("DL_COMMA")
{DL_DOT} DO_DEBUG("DL_DOT")

{ERROR} DO_DEBUG("ERR")

%%

int main(int argc, char **argv)
{
	if (argc > 1)
		yyin = fopen(argv[1], "r");
	else
		yyin = stdin;
	yylex();
	printSymTab();
}
