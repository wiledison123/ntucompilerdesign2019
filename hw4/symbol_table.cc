#include <cassert>
#include <deque>
#include <string>
#include <unordered_map>
#include <vector>

#include "header.hh"
#include "printer.hh"
#include "symbol_table.hh"

bool ScalarArrayType::operator==(const ScalarArrayType &rhs) const {
	if (dims.size() != rhs.dims.size()) return false;
	if (dims.size() == 0) return true;
	auto l_it = dims.begin(), r_it = rhs.dims.begin();
	++l_it;
	++r_it;
	for (; l_it != dims.end() && r_it != rhs.dims.end(); l_it++, r_it++) {
		if (*l_it != *r_it) return false;
	}
	return true;
}

bool FunctionType::operator==(const FunctionType &rhs) const {
	return return_type == rhs.return_type && args == rhs.args;
}

bool TypedefType::operator==(const TypedefType &rhs) const {
	return realtype == rhs.realtype;
}

void SymbolTable::InsertVariable(AstNode *now, SymbolType datatype,
                                 const std::string &name) {
	auto &mp = stack.back();
	if (mp.find(name) != mp.end()) {
		// Duplication!
		PrintError(now, "Variable " + name + " redeclared.");
	} else {
		mp[name] = datatype;
	}
}

void SymbolTable::InsertType(AstNode *now, ScalarArrayType target,
                             const std::string &name) {
	InsertVariable(now, TypedefType{target}, name);
}

SymbolType SymbolTable::GetVariableType(AstNode *now, const std::string &name) {
	(void)now;
	for (auto s_it = stack.rbegin(); s_it != stack.rend(); s_it++) {
		auto it = s_it->find(name);
		if (it != s_it->end()) {
			return it->second;
		}
	}
	return ScalarArrayType{ERROR_TYPE, {}};
}

ScalarArrayType SymbolTable::GetTypedef(AstNode *now, const std::string &name) {
	for (auto s_it = stack.rbegin(); s_it != stack.rend(); s_it++) {
		auto it = s_it->find(name);
		if (it != s_it->end()) {
			if (auto p = std::get_if<TypedefType>(&(it->second))) {
				return p->realtype;
			}
			break;
		}
	}
	PrintError(now, name + " does not name a type.");
	return ScalarArrayType{ERROR_TYPE, {}};
}

ScalarArrayType SymbolTable::CheckCallable(
    AstNode *now, const std::string &name,
    const std::vector<ScalarArrayType> &types) {
	// TODO-CLEAN: This is freaking dirty.
	SymbolType func_type;
	if (name == "write") {
		if (types.size() == 1 && types[0].dims.empty()) {
			switch (types[0].datatype) {
				case INT_TYPE:
				case FLOAT_TYPE:
				case CONST_STRING_TYPE:
					func_type = FunctionType{VOID_TYPE, {{types[0].datatype, {}}}};
					break;
				case NULL_DATATYPE:
				case VOID_TYPE:
				case INT_PTR_TYPE:
				case FLOAT_PTR_TYPE:
				case NONE_TYPE:
				case ERROR_TYPE:
					func_type = FunctionType{VOID_TYPE, {}};
			}
		} else {
			func_type = FunctionType{VOID_TYPE, {}};
		}
	} else {
		func_type = GetVariableType(now, name);
	}
	auto p = std::get_if<FunctionType>(&func_type);
	if (p) {
		if (types == p->args) {
			return {p->return_type, {}};
		} else if (types.size() < p->args.size()) {
			PrintError(now, "Too few arguments to function ", name);
		} else if (types.size() > p->args.size()) {
			PrintError(now, "Too many arguments to function ", name);
		} else {
			PrintError(now, "Argument type mismatch to function ", name);
		}
	} else {
		PrintError(now, "Function ", name, " not declared.");
	}
	return {ERROR_TYPE, {}};
}

void SymbolTable::OpenScope() { stack.emplace_back(); }

void SymbolTable::CloseScope() {
	assert(stack.size() > 1);
	stack.pop_back();
}
