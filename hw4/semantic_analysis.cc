#include "semantic_analysis.hh"
#include "printer.hh"

bool has_error;

#ifdef DEBUG
bool debug = 1;
#else
bool debug = 0;
#endif

// TODO-CLEAN: We should place all of these into a class.
static SymbolTable symbol_table;
static ScalarArrayType current_function_return_type;

static bool CheckTruthable(const ScalarArrayType &type) {
	auto t = type.datatype;
	return type.dims.empty() && !(t == NULL_DATATYPE || t == VOID_TYPE ||
	                              t == NONE_TYPE || t == ERROR_TYPE);
}

static void DeleteAstNode(AstNode *&z) {
	if (z->child) DeleteAstNode(z->child);
	if (z->rightSibling) DeleteAstNode(z->rightSibling);
	delete z;
	z = nullptr;
}

void ConstFold(AstNode *root) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wfloat-equal"
	root->nodeType = CONST_VALUE_NODE;
	auto value = std::get<ExprSemanticVal>(root->semantic_value);
	if (value.kind == BINARY_OPERATION) {
		std::visit(
			overload{[&](auto x, auto y) -> void {
						 switch (value.op) {
							 case BINARY_OP_ADD:
								 root->semantic_value = x + y;
								 break;
							 case BINARY_OP_SUB:
								 root->semantic_value = x - y;
								 break;
							 case BINARY_OP_MUL:
								 root->semantic_value = x * y;
								 break;
							 case BINARY_OP_DIV:
								 if (y != 0) root->semantic_value = x / y;
								 break;
							 case BINARY_OP_EQ:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x == y;
								 break;
							 case BINARY_OP_GE:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x >= y;
								 break;
							 case BINARY_OP_LE:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x <= y;
								 break;
							 case BINARY_OP_NE:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x != y;
								 break;
							 case BINARY_OP_GT:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x > y;
								 break;
							 case BINARY_OP_LT:
								 root->dataType = INT_TYPE;
								 root->semantic_value = x < y;
								 break;
							 case BINARY_OP_AND:
								 root->dataType = INT_TYPE;
								 root->semantic_value =
									 static_cast<bool>(x) && static_cast<bool>(y);
								 break;
							 case BINARY_OP_OR:
								 root->dataType = INT_TYPE;
								 root->semantic_value =
									 static_cast<bool>(x) || static_cast<bool>(y);
								 break;
							 case UNARY_OP_POSITIVE:
							 case UNARY_OP_NEGATIVE:
							 case UNARY_OP_LOGICAL_NEGATION:
								 assert(0 && "Unary operator not expected.");
						 }
					 },
					 [](int x, std::string y) -> void {},
					 [](double x, std::string y) -> void {},
					 [](std::string x, int y) -> void {},
					 [](std::string x, double y) -> void {},
					 [](std::string x, std::string y) -> void {}},
			std::get<Constant>(root->child->semantic_value),
			std::get<Constant>(root->child->rightSibling->semantic_value));
	} else {
		std::visit(
			overload{[&](auto x) -> void {
						 switch (value.op) {
							 case UNARY_OP_POSITIVE:
								 root->semantic_value = x;
								 break;
							 case UNARY_OP_NEGATIVE:
							 	 root->semantic_value = -x;
								 break;
							 case UNARY_OP_LOGICAL_NEGATION:
							 	 root->dataType = INT_TYPE;
								 root->semantic_value = !static_cast<bool>(x);
								 break;
							 case BINARY_OP_ADD:
							 case BINARY_OP_SUB:
							 case BINARY_OP_MUL:
							 case BINARY_OP_DIV:
							 case BINARY_OP_EQ:
							 case BINARY_OP_GE:
							 case BINARY_OP_LE:
							 case BINARY_OP_NE:
							 case BINARY_OP_GT:
							 case BINARY_OP_LT:
							 case BINARY_OP_AND:
							 case BINARY_OP_OR:
								 assert(0 && "Binary operator not expected.");
						 }
					 },
					 [](std::string x) -> void {}},
			std::get<Constant>(root->child->semantic_value));
	}
	DeleteAstNode(root->child);
#pragma GCC diagnostic pop
}

void ProcessExprNode(AstNode *root) {
	assert(root->nodeType == EXPR_NODE);
	auto value = std::get<ExprSemanticVal>(root->semantic_value);
	if (value.kind == BINARY_OPERATION) {
		AstNode *c1 = root->child, *c2 = root->child->rightSibling;
		ProcessNode(c1);
		ProcessNode(c2);
		DataType type1 = c1->dataType.datatype;
		DataType type2 = c2->dataType.datatype;
		if (c1->dataType.dims.size() || c2->dataType.dims.size()) {
			PrintError(root, "Arithmetic of array types.");
			root->dataType = ERROR_TYPE;
		} else if (type1 == ERROR_TYPE || type2 == ERROR_TYPE) {
			root->dataType = ERROR_TYPE;
		} else {
			if ((type1 != INT_TYPE && type1 != FLOAT_TYPE) ||
			    (type2 != INT_TYPE && type2 != FLOAT_TYPE)) {
				root->dataType = ERROR_TYPE;
				PrintError(root, "Error type to perform operator ", Operator2str[value.op]);
			} else {
				DataType type =
				    (type1 == INT_TYPE && type2 == INT_TYPE ? INT_TYPE : FLOAT_TYPE);
				root->dataType = {type, {}};
				if (c1->nodeType == CONST_VALUE_NODE &&
				    c2->nodeType == CONST_VALUE_NODE) 
					ConstFold(root);
			}
		}
	} else {
		ProcessNode(root->child);
		if (root->child->dataType.dims.size()) {
			PrintError(root, "Arithmetic of array types.");
			root->dataType = ERROR_TYPE;
		}
		if (root->child->dataType.datatype == ERROR_TYPE) {
			root->dataType = ERROR_TYPE;
		} else {
			DataType type = root->child->dataType.datatype;
			if (type != INT_TYPE && type != FLOAT_TYPE) {
				root->dataType = ERROR_TYPE;
				PrintError(root, "Error type to perform operator ", Operator2str[value.op]);
			} else {
				root->dataType = {root->child->dataType.datatype, {}};
				if (root->child->nodeType == CONST_VALUE_NODE)
					ConstFold(root);
			}
		}
	}

	if (root->dataType.datatype == ERROR_TYPE) DeleteAstNode(root->child);
}

void ProcessNonEmptyAssignExprListNode(AstNode *root) {
	assert(root->nodeType == NONEMPTY_ASSIGN_EXPR_LIST_NODE);
	root = root->child;
	while (root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == ASSIGN_STMT);
		ProcessNode(root);
		root = root->rightSibling;
	}
}

std::vector<ScalarArrayType> ProcessNonEmptyRelopExprListNode(AstNode *root) {
	assert(root->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE);
	root = root->child;
	std::vector<ScalarArrayType> res;
	while (root) {
		ProcessNode(root);
		res.push_back(root->dataType);
		root = root->rightSibling;
	}
	return res;
}

void CheckAssignable(AstNode *now, const ScalarArrayType &lhs,
                     const ScalarArrayType &rhs, bool check_return = false) {
	if (lhs.dims.size() || rhs.dims.size()) {
		PrintError(now, "Arrays not re-assignable.");
	}
	auto l = lhs.datatype, r = rhs.datatype;
	if (l == INT_TYPE && r == INT_TYPE)
		;
	else if (l == FLOAT_TYPE && r == INT_TYPE)
		;
	else if (l == INT_TYPE && r == FLOAT_TYPE)
		;
	else if (l == FLOAT_TYPE && r == FLOAT_TYPE)
		;
	else if (l == VOID_TYPE && r == VOID_TYPE)
		;
	else {
		if (check_return)
			PrintWarning(now, "Type error to return: returning ", DataType2str[r], " in function which returns ", DataType2str[l]);
		else 
			PrintError(now, "Type error to assign: assigning ", DataType2str[r], " to ", DataType2str[l]);
	}
}

void ProcessReturnStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind == RETURN_STMT);
	root = root->child;
	ProcessNode(root);
	CheckAssignable(root, current_function_return_type, root->dataType, true);
}

void ProcessFunctionCallStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind ==
	           FUNCTION_CALL_STMT);
	auto func_root = root;
	root = root->child;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root = root->rightSibling;
	std::vector<ScalarArrayType> types;
	if (root->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE)
		types = ProcessNonEmptyRelopExprListNode(root);
	func_root->dataType = symbol_table.CheckCallable(root, name, types);
}

void ProcessIfStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind == IF_STMT);
	root = root->child;
	ProcessNode(root);
	if (!CheckTruthable(root->dataType)) {
		PrintError(root, "If can not test type: ", DataType2str[root->dataType.datatype]);
	}
	ProcessNode(root->rightSibling);
	ProcessNode(root->rightSibling->rightSibling);
}

void ProcessAssignStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind == ASSIGN_STMT);
	root = root->child;
	ProcessNode(root);
	ProcessNode(root->rightSibling);
	CheckAssignable(root, root->dataType, root->rightSibling->dataType);
}

void ProcessForStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind == FOR_STMT);
	root = root->child;
	ProcessNode(root);  // Init
	root = root->rightSibling;
	ProcessNode(root);  // Condition
	if (!CheckTruthable(root->dataType)) 
		PrintError(root, "For can not test type: ", DataType2str[root->dataType.datatype]);
	root = root->rightSibling;
	ProcessNode(root);  // Increment
	root = root->rightSibling;
	ProcessNode(root);  // Block
}

void ProcessWhileStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE &&
	       std::get<StmtSemanticVal>(root->semantic_value).kind == WHILE_STMT);
	root = root->child;
	ProcessNode(root);
	if (!CheckTruthable(root->dataType)) {
		PrintError(root, "While can not test type: ", DataType2str[root->dataType.datatype]);
	}
	root = root->rightSibling;
	ProcessNode(root);
}

void ProcessNulNode(AstNode *root) {
	root->dataType = VOID_TYPE;
}

void ProcessConstValueNode(AstNode *root) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
	root->dataType =
	    std::visit(overload{[](double x) { return FLOAT_TYPE; },
	                        [](int x) { return INT_TYPE; },
	                        [](std::string x) { return CONST_STRING_TYPE; }},
	               std::get<Constant>(root->semantic_value));
#pragma GCC diagnostic pop
}

void ProcessIdentifierNode(AstNode *root) {
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto type =
	    std::get<ScalarArrayType>(symbol_table.GetVariableType(root, name));
	if (type.datatype == ERROR_TYPE)
		PrintError(root, name, " was not declared here.");
	else if (std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID) {
		AstNode *child = root->child;
		while (child) {
			// TODO: This is in quadratic complexity.
			if (type.dims.size()) {
				type.dims.erase(type.dims.begin());
				child = child->rightSibling;
			} else {
				PrintError(root, "Attempt to index into a scalar.");
				break;
			}
		}
	}
	root->dataType = type;
}

void ProcessStatementNode(AstNode *root) {
	assert(root->nodeType == STMT_NODE);
	int knd = std::get<StmtSemanticVal>(root->semantic_value).kind;
	switch (knd) {
		case WHILE_STMT:
			ProcessWhileStatementNode(root);
			break;
		case FOR_STMT:
			ProcessForStatementNode(root);
			break;
		case ASSIGN_STMT:
			ProcessAssignStatementNode(root);
			break;
		case IF_STMT:
			ProcessIfStatementNode(root);
			break;
		case FUNCTION_CALL_STMT:
			ProcessFunctionCallStatementNode(root);
			break;
		case RETURN_STMT:
			ProcessReturnStatementNode(root);
			break;
	}
}

void ProcessStatementListNode(AstNode *root) {
	assert(root->nodeType == STMT_LIST_NODE);
	root = root->child;
	while (root) {
		if (root->nodeType == BLOCK_NODE)
			ProcessBlockNode(root);
		else if (root->nodeType == STMT_NODE)
			ProcessStatementNode(root);
		else
			assert(0);
		root = root->rightSibling;
	}
}

void ProcessBlockNode(AstNode *root, bool open_scope) {
	assert(root->nodeType == BLOCK_NODE);
	if (open_scope) symbol_table.OpenScope();
	root = root->child;
	while (root) {
		if (root->nodeType == VARIABLE_DECL_LIST_NODE)
			ProcessVariableDeclarationListNode(root);
		else if (root->nodeType == STMT_LIST_NODE)
			ProcessStatementListNode(root);
		else
			assert(0);
		root = root->rightSibling;
	}
	symbol_table.CloseScope();
}

std::vector<std::pair<ScalarArrayType, std::string>> CollectParamDeclarationInfo(
    AstNode *root) {
	assert(root->nodeType == PARAM_LIST_NODE);
	std::vector<std::pair<ScalarArrayType, std::string>> result;
	root = root->child;
	while (root) {
		assert(root->nodeType == DECLARATION_NODE &&
		       std::get<DeclSemanticVal>(root->semantic_value).kind ==
		           FUNCTION_PARAMETER_DECL);
		AstNode *now = root->child;
		std::string type =
		    std::get<IdSemanticVal>(now->semantic_value).identifierName;
		auto real_type = symbol_table.GetTypedef(root, type);
		now = now->rightSibling;
		std::string name =
		    std::get<IdSemanticVal>(now->semantic_value).identifierName;
		if (int knd = std::get<IdSemanticVal>(now->semantic_value).kind;
		    knd == NORMAL_ID) {
			result.emplace_back(real_type, name);
		} else if (knd == ARRAY_ID) {
			now = now->child;
			std::vector<int> dims = CollectDimInfo(now);
			if (dims.size()) {
				real_type.dims.insert(real_type.dims.begin(), dims.begin(), dims.end());
				result.emplace_back(real_type, name);
			} else {
				// Error, but we push something in anyway.
				result.emplace_back(real_type, name);
			}
		}
		root = root->rightSibling;
	}
	return result;
}

void ProcessFunctionDeclarationNode(AstNode *root) {
	assert(root->nodeType == DECLARATION_NODE);
	root = root->child;
	std::string type =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(root, type);
	if (real_type.dims.size()) {
		PrintError(root, "Return value has to be a scalar.");
	}
	current_function_return_type = real_type;
	root = root->rightSibling;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root = root->rightSibling;
	auto param = CollectParamDeclarationInfo(root);
	std::vector<ScalarArrayType> param_type;
	for (auto &par : param) param_type.push_back(par.first);
	symbol_table.InsertVariable(
	    root, FunctionType{real_type.datatype, param_type}, name);
	symbol_table.OpenScope();
	for (auto &par : param)
		symbol_table.InsertVariable(root, par.first, par.second);
	root = root->rightSibling;
	ProcessBlockNode(root, false);
	current_function_return_type = NONE_TYPE;
}

std::vector<int> CollectDimInfo(AstNode *root) {
	std::vector<int> dims;
	while (root) {
		ProcessNode(root);
		if (!std::holds_alternative<Constant>(root->semantic_value)) {
			PrintError(root, "Value in dimentions is not constant.");
			return std::vector<int>();
		}
		auto &cnst = std::get<Constant>(root->semantic_value);
		if (!std::holds_alternative<int>(cnst)) {
			PrintError(root, "Value in dimensions should have type int.");
			return std::vector<int>();
		}
		dims.push_back(std::get<int>(cnst));
		root = root->rightSibling;
	}
	return dims;
}

void ProcessDeclarationNode(AstNode *root) {
	assert(root->nodeType == DECLARATION_NODE &&
	       std::get<DeclSemanticVal>(root->semantic_value).kind == VARIABLE_DECL);
	root = root->child;
	std::string type =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(root, type);
	while ((root = root->rightSibling)) {
		auto cur_type = real_type;
		std::string name =
		    std::get<IdSemanticVal>(root->semantic_value).identifierName;
		if (int knd = std::get<IdSemanticVal>(root->semantic_value).kind;
		    knd == WITH_INIT_ID) {
			symbol_table.InsertVariable(root, cur_type, name);
			ProcessNode(root);
			ProcessNode(root->child);
			CheckAssignable(root, root->dataType, root->child->dataType);
		} else if (knd == NORMAL_ID) {
			symbol_table.InsertVariable(root, cur_type, name);
		} else {
			std::vector<int> dims = CollectDimInfo(root->child);
			cur_type.dims.insert(cur_type.dims.begin(), dims.begin(), dims.end());
			symbol_table.InsertVariable(root, cur_type, name);
		}
	}
}

void ProcessTypeDeclarationNode(AstNode *root) {
	assert(root->nodeType == DECLARATION_NODE &&
	       std::get<DeclSemanticVal>(root->semantic_value).kind == TYPE_DECL);
	root = root->child;
	std::string type =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(root, type);
	while ((root = root->rightSibling)) {
		std::string name =
		    std::get<IdSemanticVal>(root->semantic_value).identifierName;
		if (std::get<IdSemanticVal>(root->semantic_value).kind == NORMAL_ID) {
			assert(!root->child && "Value assignment unexpected.");
			symbol_table.InsertType(root, real_type, name);
		} else {
			// TODO: Declare array in typedef?
			PrintError(root, "It is forbidden to declare an array in type declaration");
		}
	}
}

void ProcessVariableDeclarationListNode(AstNode *root) {
	assert(root->nodeType == VARIABLE_DECL_LIST_NODE);
	root = root->child;
	while (root) {
		if (std::get<DeclSemanticVal>(root->semantic_value).kind == VARIABLE_DECL)
			ProcessDeclarationNode(root);
		else
			ProcessTypeDeclarationNode(root);
		root = root->rightSibling;
	}
}

void SemanticAnalysis(AstNode *root) {
	assert(root->nodeType == PROGRAM_NODE);
	if (debug) PrintAst(std::cerr, 0, root);
	root = root->child;
	while (root) {
		if (root->nodeType == DECLARATION_NODE)
			ProcessFunctionDeclarationNode(root);
		else if (root->nodeType == VARIABLE_DECL_LIST_NODE)
			ProcessVariableDeclarationListNode(root);
		root = root->rightSibling;
	}
}

void ProcessNode(AstNode *root) {
	switch (root->nodeType) {
		case DECLARATION_NODE:
			ProcessDeclarationNode(root);
			break;
		case IDENTIFIER_NODE:
			ProcessIdentifierNode(root);
			break;
		case NUL_NODE:
			ProcessNulNode(root);
			break;
		case BLOCK_NODE:
			ProcessBlockNode(root);
			break;
		case STMT_NODE:
			ProcessStatementNode(root);
			break;
		case STMT_LIST_NODE:
			ProcessStatementListNode(root);
			break;
		case EXPR_NODE:
			ProcessExprNode(root);
			break;
		case CONST_VALUE_NODE:
			ProcessConstValueNode(root);
			break;
		case NONEMPTY_ASSIGN_EXPR_LIST_NODE:
			ProcessNonEmptyAssignExprListNode(root);
			break;
		// case NONEMPTY_RELOP_EXPR_LIST_NODE:
		// ProcessNonEmptyRelopExprListNode(root); break;
		case NULL_ASTTYPE:
		case PROGRAM_NODE:
		case PARAM_LIST_NODE:
		case VARIABLE_DECL_LIST_NODE:
		case NONEMPTY_RELOP_EXPR_LIST_NODE:
			PrintError(root, "This is a bug: Unsupported node type: ", AstType2str[root->nodeType]);
			break;
	}
}
