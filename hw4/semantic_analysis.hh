#ifndef SEMANTIC_ANALYSIS_HH_
#define SEMANTIC_ANALYSIS_HH_

#include <cstdio>

#include "header.hh"
#include "symbol_table.hh"
#include "printer.hh"

std::vector<int> CollectDimInfo(AstNode *root);
void ProcessNode(AstNode *root);
void ProcessAssignExprNode(AstNode *root);
void ProcessReturnStatementNode(AstNode *root);
void ProcessFunctionCallStatementNode(AstNode *root);
void ProcessIfStatementNode(AstNode *root);
void ProcessAssignStatementNode(AstNode *root);
void ProcessForStatementNode(AstNode *root);
void ProcessWhileStatementNode(AstNode *root);
void ProcessStatementNode(AstNode *root);
void ProcessStatementListNode(AstNode *root);
void ProcessBlockNode(AstNode *root, bool open_scope = true);
void ProcessFunctionDeclarationNode(AstNode *root);
void ProcessVariableDeclarationNode(AstNode *root);
void ProcessVariableDeclarationListNode(AstNode *root);

void SemanticAnalysis(AstNode *root);

#endif
