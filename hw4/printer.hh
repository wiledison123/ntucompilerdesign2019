#ifndef PRINTER_H_
#define PRINTER_H_

#include "header.hh"

#include <iostream>

template <typename T>
void PrintDetail(T t) {
	std::cerr << t;
}
template <typename T, typename... Args>
void PrintDetail(T t, Args... args) {
	std::cerr << t;
	PrintDetail(args...);
}

template <typename T>
void PrintWarning(AstNode *now, T t) {
	std::cerr << "Warning: In line " << now->line_number << ", " << t << std::endl;
}
template <typename... Args>
void PrintWarning(AstNode *now, Args... args) {
	std::cerr << "Warning: In line " << now->line_number << ", ";
	PrintDetail(args...);
	std::cerr << std::endl;
}

template <typename T>
void PrintError(AstNode *now, T t) {
	has_error = true;
	std::cerr << "Error: In line " << now->line_number << ", " << t << std::endl;
}
template <typename... Args>
void PrintError(AstNode *now, Args... args) {
	has_error = true;
	std::cerr << "Error: In line " << now->line_number << ", ";
	PrintDetail(args...);
	std::cerr << std::endl;
}

void PrintAst(std::ostream &o, int dep, AstNode *now);

#endif
