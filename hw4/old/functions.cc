/* 2015/10 functions to support printGV() */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <variant>

#include "header.h"

const char *AST_TYPE_string[] = {"PROGRAM",
                                 "GLOBAL_DECL_LIST",
                                 "GLOBAL_DECL",
                                 "DECL_LIST",
                                 "FUNCTION_DECL",
                                 "PARAM_LIST",
                                 "PARAM",
                                 "DIM_FN",
                                 "DIMFN1",
                                 "EXPR_nullptr",
                                 "BLOCK",
                                 "DECL",
                                 "TYPE_DECL",
                                 "VAR_DECL",
                                 "TYPE",
                                 "STRUCT_TYPE",
                                 "DEF_LIST",
                                 "DEF",
                                 "OPT_TAG",
                                 "TAG",
                                 "ID_LIST",
                                 "DIM_DECL",
                                 "CEXPR",
                                 "MCEXPR",
                                 "CFACTOR",
                                 "INIT_ID_LIST",
                                 "INIT_ID",
                                 "STMT_LIST",
                                 "STMT",
                                 "ASSIGN_EXPR_LIST",
                                 "NONEMPTY_ASSIGN_EXPR_LIST",
                                 "TEST",
                                 "ASSIGN_EXPR",
                                 "RELOP_EXPR",
                                 "RELOP_TERM",
                                 "RELOP_FACTOR",
                                 "REL_OP",
                                 "RELOP_EXPR_LIST",
                                 "NONEMPTY_RELOP_EXPR_LIST",
                                 "EXPR",
                                 "ADD_OP",
                                 "TERM",
                                 "MUL_OP",
                                 "FACTOR",
                                 "VAR_REF",
                                 "DIM",
                                 "STRUCT_TAIL",
                                 "NUL",
                                 "ID_value",
                                 "CONST_value"};

int printGVNode(FILE *fp, AstNode *node, int count);

void printLabelString(FILE *fp, AstNode *astNode) {
	const char *binaryOpString[] = {
	    "+", "-", "*", "/", "==", ">=", "<=", "!=", ">", "<", "&&", "||"};
	const char *unaryOpString[] = {"+", "-", "!"};
	//    fprintf(fp, "%d ", astNode->linenumber);
	switch (astNode->nodeType) {
		case PROGRAM_NODE:
			fprintf(fp, "PROGRAM_NODE");
			break;
		case DECLARATION_NODE:
			fprintf(fp, "DECLARATION_NODE ");
			switch (std::get<DeclSemanticVal>(astNode->semantic_value).kind) {
				case VARIABLE_DECL:
					fprintf(fp, "VARIABLE_DECL");
					break;
				case TYPE_DECL:
					fprintf(fp, "TYPE_DECL");
					break;
				case FUNCTION_DECL:
					fprintf(fp, "FUNCTION_DECL");
					break;
				case FUNCTION_PARAMETER_DECL:
					fprintf(fp, "FUNCTION_PARAMETER_DECL");
					break;
			}
			break;
		case IDENTIFIER_NODE:
			fprintf(fp, "IDENTIFIER_NODE ");
			fprintf(fp, "%s ",
			        std::get<IdSemanticVal>(astNode->semantic_value)
			            .identifierName.c_str());
			switch (std::get<IdSemanticVal>(astNode->semantic_value).kind) {
				case NORMAL_ID:
					fprintf(fp, "NORMAL_ID");
					break;
				case ARRAY_ID:
					fprintf(fp, "ARRAY_ID");
					break;
				case WITH_INIT_ID:
					fprintf(fp, "WITH_INIT_ID");
					break;
			}
			break;
		case PARAM_LIST_NODE:
			fprintf(fp, "PARAM_LIST_NODE");
			break;
		case NUL_NODE:
			fprintf(fp, "NUL_NODE");
			break;
		case BLOCK_NODE:
			fprintf(fp, "BLOCK_NODE");
			break;
		case VARIABLE_DECL_LIST_NODE:
			fprintf(fp, "VARIABLE_DECL_LIST_NODE");
			break;
		case STMT_LIST_NODE:
			fprintf(fp, "STMT_LIST_NODE");
			break;
		case STMT_NODE:
			fprintf(fp, "STMT_NODE ");
			switch (std::get<StmtSemanticVal>(astNode->semantic_value).kind) {
				case WHILE_STMT:
					fprintf(fp, "WHILE_STMT");
					break;
				case FOR_STMT:
					fprintf(fp, "FOR_STMT");
					break;
				case ASSIGN_STMT:
					fprintf(fp, "ASSIGN_STMT");
					break;
				case IF_STMT:
					fprintf(fp, "IF_STMT");
					break;
				case FUNCTION_CALL_STMT:
					fprintf(fp, "FUNCTION_CALL_STMT");
					break;
				case RETURN_STMT:
					fprintf(fp, "RETURN_STMT");
					break;
			}
			break;
		case EXPR_NODE:
			fprintf(fp, "EXPR_NODE ");
			switch (std::get<ExprSemanticVal>(astNode->semantic_value).kind) {
				case BINARY_OPERATION:
					fprintf(
					    fp, "%s",
					    binaryOpString[std::get<ExprSemanticVal>(astNode->semantic_value)
					                       .op]);
					break;
				case UNARY_OPERATION:
					fprintf(
					    fp, "%s",
					    unaryOpString[std::get<ExprSemanticVal>(astNode->semantic_value)
					                      .op -
					                  UNARY_BASE]);
					break;
			}
			break;
		case CONST_VALUE_NODE:
			fprintf(fp, "CONST_VALUE_NODE ");
			std::visit(overload{[&](double x) { fprintf(fp, "%f", x); },
			                    [&](int x) { fprintf(fp, "%d", x); },
			                    [&](std::string x) { fprintf(fp, "%s", x.c_str()); }},
			           std::get<Constant>(astNode->semantic_value));
			break;
		case NONEMPTY_ASSIGN_EXPR_LIST_NODE:
			fprintf(fp, "NONEMPTY_ASSIGN_EXPR_LIST_NODE");
			break;
		case NONEMPTY_RELOP_EXPR_LIST_NODE:
			fprintf(fp, "NONEMPTY_RELOP_EXPR_LIST_NODE");
			break;
	}
}

void printGV(AstNode *root, char *fileName) {
	if (fileName == nullptr) {
		fileName = strdup("AST_Graph.gv");
	}
	FILE *fp;
	fp = fopen(fileName, "w");
	if (!fp) {
		printf("Cannot open file \"%s\"\n", fileName);
		return;
	}
	fprintf(fp, "Digraph AST\n");
	fprintf(fp, "{\n");
	fprintf(fp, "label = \"%s\"\n", fileName);

	int nodeCount = 0;
	printGVNode(fp, root, nodeCount);

	fprintf(fp, "}\n");
	fclose(fp);
}

// count: the (unused) id number to be used
// return: then next unused id number
int printGVNode(FILE *fp, AstNode *node, int count) {
	if (node == nullptr) {
		return count;
	}

	int currentNodeCount = count;
	fprintf(fp, "node%d [label =\"", count);
	printLabelString(fp, node);
	fprintf(fp, "\"]\n");
	++count;
	int countAfterCheckChildren = count;
	if (node->child) {
		countAfterCheckChildren = printGVNode(fp, node->child, count);
		fprintf(fp, "node%d -> node%d [style = bold]\n", currentNodeCount, count);
	}

	int countAfterCheckSibling = countAfterCheckChildren;
	if (node->rightSibling) {
		countAfterCheckSibling =
		    printGVNode(fp, node->rightSibling, countAfterCheckChildren);
		fprintf(fp, "node%d -> node%d [style = dashed]\n", currentNodeCount,
		        countAfterCheckChildren);
	}

	return countAfterCheckSibling;
}
