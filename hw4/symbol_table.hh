#ifndef SYMBOL_TABLE_HH_
#define SYMBOL_TABLE_HH_

#include <cassert>
#include <deque>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

#include "header.hh"

class SymbolTable {
 public:
	using HashTable = std::unordered_map<std::string, SymbolType>;

	SymbolTable() : stack() {
		HashTable init;
		init["int"] = TypedefType{{INT_TYPE, {}}};
		init["float"] = TypedefType{{FLOAT_TYPE, {}}};
		init["void"] = TypedefType{{VOID_TYPE, {}}};
		init["read"] = FunctionType{INT_TYPE, {}};
		init["fread"] = FunctionType{FLOAT_TYPE, {}};
		stack.emplace_back(std::move(init));
	}

	void InsertVariable(AstNode *now, SymbolType datatype, const std::string &name);

	void InsertType(AstNode *now, ScalarArrayType target, const std::string &name);

	SymbolType GetVariableType(AstNode *now, const std::string &name);

	ScalarArrayType GetTypedef(AstNode *now, const std::string &name);

	ScalarArrayType CheckCallable(AstNode *now, const std::string &name,
	                       const std::vector<ScalarArrayType> &types);

	void OpenScope();

	void CloseScope();

 private:
	std::deque<HashTable> stack;  // Deque for random access
};

#endif
