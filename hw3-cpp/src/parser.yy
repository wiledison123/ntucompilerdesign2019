/* ===== Definition Section ===== */

%skeleton "lalr1.cc"
%require "3.4"
%language "c++"
%define api.token.constructor
%define api.token.prefix {TOK_}
%define api.value.type variant
%define parse.assert
%printer { yyo << $$; } <*>;

%{
#include <cassert>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>

#include "header.hh"
#include "parser.hh"

void printGV(AstNode *root, char *fileName);

#define YY_DECL yy::parser::symbol_type yylex(void)
YY_DECL;

int line_number = 1;
AstNode *prog;

static inline AstNode *MakeSibling(AstNode *a, AstNode *b) {
  while (a->right_sibling) {
    a = a->right_sibling;
  }
  if (b == nullptr) {
    return a;
  }
  b = b->leftmost_sibling;
  a->right_sibling = b;

  b->leftmost_sibling = a->leftmost_sibling;
  b->parent = a->parent;
  while (b->right_sibling) {
    b = b->right_sibling;
    b->leftmost_sibling = a->leftmost_sibling;
    b->parent = a->parent;
  }
  return b;
}

static inline AstNode *MakeSibling2(AstNode *a, AstNode *b) {
  MakeSibling(a, b);
  return a;
}

static inline AstNode *MakeChild(AstNode *parent, AstNode *child) {
  if (child == nullptr) {
    return parent;
  }
  if (parent->child) {
    MakeSibling(parent->child, child);
  } else {
    child = child->leftmost_sibling;
    parent->child = child;
    while (child) {
      child->parent = parent;
      child = child->right_sibling;
    }
  }
  return parent;
}

static inline AstNode *MakeFamily(AstNode *parent, int children_count, ...) {
  va_list children_list;
  va_start(children_list, children_count);
  AstNode *child = va_arg(children_list, AstNode *);
  MakeChild(parent, child);
  AstNode *tmp = child;
  int index = 1;
  for (index = 1; index < children_count; ++index) {
    child = va_arg(children_list, AstNode *);
    tmp = MakeSibling(tmp, child);
  }
  va_end(children_list);
  return parent;
}

static inline AstNode *MakeIdNode(std::string lexeme, IdKind id_kind) {
  AstNode *identifier = Allocate(IDENTIFIER_NODE);
	identifier->semantic_value = IdSemanticVal{lexeme, id_kind};
  //identifier->semantic_value.identifierSemanticValue.symbolTableEntry = nullptr;
  return identifier;
}

static inline AstNode *MakeStmtNode(StmtKind stmt_kind) {
  AstNode *stmt_node = Allocate(STMT_NODE);
	stmt_node->semantic_value = StmtSemanticVal{stmt_kind};
  return stmt_node;
}

static inline AstNode *makeDeclNode(DeclKind decl_kind) {
  AstNode *decl_node = Allocate(DECLARATION_NODE);
  decl_node->semantic_value = DeclSemanticVal{decl_kind};
  return decl_node;
}

static inline AstNode *MakeExprNode(ExprKind expr_kind,
                                     int operation_enum_value) {
  AstNode *expr_node = Allocate(EXPR_NODE);
	expr_node->semantic_value = ExprSemanticVal(expr_kind, false, Operator(operation_enum_value));
  return expr_node;
}
%}

%token <std::string> ID
%token <Constant> CONST

%token ELSE
%token FLOAT
%token FOR
%token IF
%token INT
%token RETURN
%token TYPEDEF
%token VOID
%token WHILE

%token OP_PLUS
%token OP_MINUS
%token OP_TIMES
%token OP_DIVIDE
%token OP_LT
%token OP_GT
%token OP_LE
%token OP_GE
%token OP_NE
%token OP_EQ
%token OP_OR
%token OP_AND
%token OP_NOT
%token OP_ASSIGN

%token MK_LBRACE
%token MK_RBRACE
%token MK_LBRACK
%token MK_RBRACK 
%token MK_LPAREN
%token MK_RPAREN

%token MK_SEMICOLON
%token MK_COMMA
%token MK_DOT

%token ERROR

%type <AstNode*> assign_expr assign_expr_list block cexpr cfactor decl decl_list dim_decl dim_fn dim_list expr expr_null factor function_decl global_decl global_decl_list id_list init_id init_id_list mcexpr nonempty_assign_expr_list nonempty_relop_expr_list param param_list program relop_expr relop_expr_list relop_factor relop_term stmt stmt_list term type type_decl var_decl var_ref
%type <Operator> add_op mul_op rel_op

%start program

%%

/* ===== Grammar Section ===== */

program: global_decl_list
         {
             $$ = prog = MakeChild(Allocate(PROGRAM_NODE), $1);
         }
       | %empty
         {
             $$ = prog = Allocate(PROGRAM_NODE);
         }
       ;

global_decl_list: global_decl_list global_decl
                  {
                      $$ = MakeSibling2($1, $2);
                  }
                | global_decl
                  {
                      $$ = $1;
                  }
                ;

global_decl: decl_list function_decl
             {
                 auto *vars = Allocate(VARIABLE_DECL_LIST_NODE);
                 $$ = MakeSibling2(MakeChild(vars, $1), $2);
             }
           | function_decl
             {
                 $$ = $1;
             }
           ;

function_decl: type ID MK_LPAREN param_list MK_RPAREN MK_LBRACE block MK_RBRACE
               {
                   auto *tmp = makeDeclNode(FUNCTION_DECL);
                   auto *parameterList = Allocate(PARAM_LIST_NODE);
                   MakeChild(parameterList, $4);
                   $$ = MakeFamily(tmp, 4, $1, MakeIdNode($2, NORMAL_ID), parameterList, $7);
               }
             | VOID ID MK_LPAREN param_list MK_RPAREN MK_LBRACE block MK_RBRACE
               {
                   auto *tmp = makeDeclNode(FUNCTION_DECL);
                   auto *parameterList = Allocate(PARAM_LIST_NODE);
                   MakeChild(parameterList, $4);
                   $$ = MakeFamily(tmp, 4, MakeIdNode("void", NORMAL_ID), MakeIdNode($2, NORMAL_ID), parameterList, $7);
               }
             | type ID MK_LPAREN MK_RPAREN MK_LBRACE block MK_RBRACE
               {
                   auto *tmp = makeDeclNode(FUNCTION_DECL);
                   auto *emptyParameterList = Allocate(PARAM_LIST_NODE);
                   $$ = MakeFamily(tmp, 4, $1, MakeIdNode($2, NORMAL_ID), emptyParameterList, $6);
               }
             | VOID ID MK_LPAREN MK_RPAREN MK_LBRACE block MK_RBRACE
               {
                   auto *tmp = makeDeclNode(FUNCTION_DECL);
                   auto *emptyParameterList = Allocate(PARAM_LIST_NODE);
                   $$ = MakeFamily(tmp, 4, MakeIdNode("void", NORMAL_ID), MakeIdNode($2, NORMAL_ID), emptyParameterList, $6);
               }
            ;

param_list: param_list MK_COMMA param
            {
                $$ = MakeSibling2($1, $3);
            }
          | param
            {
                $$ = $1;
            }
          ;

param: type ID
       {
           auto *tmp = makeDeclNode(FUNCTION_PARAMETER_DECL);
           $$ = MakeFamily(tmp, 2, $1, MakeIdNode($2, NORMAL_ID));
       }
     | type ID dim_fn
       {
           auto *tmp = makeDeclNode(FUNCTION_PARAMETER_DECL);
           $$ = MakeFamily(tmp, 2, $1, MakeChild(MakeIdNode($2, ARRAY_ID), $3));
       }
     ;

dim_fn: MK_LBRACK expr_null MK_RBRACK 
        {
            // THIS IS NOT IN TEST
            $$ = $2;
        }
      | dim_fn MK_LBRACK expr MK_RBRACK 
        {
            $$ = MakeSibling2($1, $3);
        }
      ;

expr_null: expr
           {
               $$ = $1;
           }
         | %empty
           {
               $$ = Allocate(NUL_NODE);
           }
         ;

block: decl_list stmt_list
       {
           auto *dl = MakeChild(Allocate(VARIABLE_DECL_LIST_NODE), $1);
           auto *sl = MakeChild(Allocate(STMT_LIST_NODE), $2);
           $$ = MakeFamily(Allocate(BLOCK_NODE), 2, dl, sl);
       }
     | stmt_list
       {
           auto *sl = MakeChild(Allocate(STMT_LIST_NODE), $1);
           $$ = MakeChild(Allocate(BLOCK_NODE), sl);
       }
     | decl_list
       {
           auto *dl = MakeChild(Allocate(VARIABLE_DECL_LIST_NODE), $1);
           $$ = MakeChild(Allocate(BLOCK_NODE), dl);
       }
     | %empty
       {
           $$ = Allocate(BLOCK_NODE);
       }
     ;

decl_list: decl_list decl
           {
               $$ = MakeSibling2($1, $2);
           }
         | decl
           {
               $$ = $1;
           }
         ;

decl: type_decl
      {
          $$ = $1;
      }
    | var_decl
      {
          $$ = $1;
      }
    ;

type_decl: TYPEDEF type id_list MK_SEMICOLON
           {
               $$ = MakeFamily(makeDeclNode(TYPE_DECL), 2, $2, $3);
           }
         | TYPEDEF VOID id_list MK_SEMICOLON
           {
               $$ = MakeFamily(makeDeclNode(TYPE_DECL), 2, MakeIdNode("void", NORMAL_ID), $3);
           }
         ;

var_decl: type init_id_list MK_SEMICOLON
          {
              $$ = MakeFamily(makeDeclNode(VARIABLE_DECL), 2, $1, $2);

          }
        | ID id_list MK_SEMICOLON
          {
              $$ = MakeFamily(makeDeclNode(VARIABLE_DECL), 2, MakeIdNode($1, NORMAL_ID), $2);
          }
        ;

type: INT
      {
          $$ = MakeIdNode("int", NORMAL_ID);
      }
    | FLOAT
      {
          $$ = MakeIdNode("float", NORMAL_ID);
      }
    ;

id_list: ID
         {
             $$ = MakeIdNode($1, NORMAL_ID);
         }
       | id_list MK_COMMA ID
         {
             $$ = MakeSibling2($1, MakeIdNode($3, NORMAL_ID));
         }
       | id_list MK_COMMA ID dim_decl
         {
             $$ = MakeSibling2($1, MakeChild(MakeIdNode($3, ARRAY_ID), $4));
         }
       | ID dim_decl
         {
             $$ = MakeChild(MakeIdNode($1, ARRAY_ID), $2);
         }
       ;

dim_decl: MK_LBRACK cexpr MK_RBRACK 
          {
              $$ = $2;
          }
        | dim_decl MK_LBRACK cexpr MK_RBRACK 
          {
              $$ = MakeSibling2($1, $3);
          }
        ;

// This is for array declaration
cexpr: cexpr OP_PLUS mcexpr
       {
           $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_ADD), 2, $1, $3);
       }
     | cexpr OP_MINUS mcexpr
       {
           $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_SUB), 2, $1, $3);
       }
     | mcexpr
       {
           $$ = $1;
       }
     ;

mcexpr: mcexpr OP_TIMES cfactor
        {
            $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_MUL), 2, $1, $3);
        }
      | mcexpr OP_DIVIDE cfactor
        {
            $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_DIV), 2, $1, $3);
        }
      | cfactor
        {
            $$ = $1;
        }
      ;

cfactor: CONST
         {
             auto *tmp = Allocate(CONST_VALUE_NODE);
             tmp->semantic_value = $1;
             $$ = tmp;
         }
       | MK_LPAREN cexpr MK_RPAREN
         {
             $$ = $2;
         }
       ;

init_id_list: init_id
              {
                  $$ = $1;
              }
            | init_id_list MK_COMMA init_id
              {
                  $$ = MakeSibling2($1, $3);
              }
            ;

init_id: ID
         {
             $$ = MakeIdNode($1, NORMAL_ID);
         }
       | ID dim_decl
         {
             $$ = MakeChild(MakeIdNode($1, ARRAY_ID), $2);
         }
       | ID OP_ASSIGN relop_expr
         {
             $$ = MakeChild(MakeIdNode($1, WITH_INIT_ID), $3);
         }
       ;

stmt_list: stmt_list stmt
           {
               $$ = MakeSibling($1, $2);
           }
         | stmt
           {
               $$ = $1;
           }
         ;

stmt: MK_LBRACE block MK_RBRACE
      {
          $$ = $2;
      }
    | WHILE MK_LPAREN nonempty_relop_expr_list MK_RPAREN stmt
      {
          $$ = MakeFamily(MakeStmtNode(WHILE_STMT), 2, $3, $5);
      }
    | FOR MK_LPAREN assign_expr_list MK_SEMICOLON relop_expr_list MK_SEMICOLON assign_expr_list MK_RPAREN stmt
      {
          $$ = MakeFamily(MakeStmtNode(FOR_STMT), 4, $3, $5, $7, $9);
      }
    | var_ref OP_ASSIGN relop_expr MK_SEMICOLON
      {
          $$ = MakeFamily(MakeStmtNode(ASSIGN_STMT), 2, $1, $3);
      }
    | IF MK_LPAREN relop_expr MK_RPAREN stmt
      {
          $$ = MakeFamily(MakeStmtNode(IF_STMT), 3, $3, $5, Allocate(NUL_NODE));
      }
    | IF MK_LPAREN relop_expr MK_RPAREN stmt ELSE stmt
      {
          $$ = MakeFamily(MakeStmtNode(IF_STMT), 3, $3, $5, $7);
      }
    | ID MK_LPAREN relop_expr_list MK_RPAREN MK_SEMICOLON
      {
          $$ = MakeFamily(MakeStmtNode(FUNCTION_CALL_STMT), 2, MakeIdNode($1, NORMAL_ID), $3);
      }
    | MK_SEMICOLON
      {
          $$ = Allocate(NUL_NODE);
      }
    | RETURN MK_SEMICOLON
      {
          // Out of test, do this need a child?
          $$ = MakeStmtNode(RETURN_STMT);
      }
    | RETURN relop_expr MK_SEMICOLON
      {
          $$ = MakeChild(MakeStmtNode(RETURN_STMT), $2);
      }
    ;

assign_expr_list: nonempty_assign_expr_list
                  {
                      $$ = MakeChild(Allocate(NONEMPTY_ASSIGN_EXPR_LIST_NODE), $1);
                  }
                | %empty
                  {
                      $$ = Allocate(NUL_NODE);
                  }
                ;

nonempty_assign_expr_list: nonempty_assign_expr_list MK_COMMA assign_expr
                           {
                               $$ = MakeSibling2($1, $3);
                           }
                         | assign_expr
                           {
                               $$ = $1;
                           }
                         ;

/* === Unused === //
test: assign_expr
      {
          $$ = $1;
      }
    ;
// === Unused === */

assign_expr: ID OP_ASSIGN relop_expr
             {
                 $$ = MakeFamily(MakeStmtNode(ASSIGN_STMT), 2, MakeIdNode($1, NORMAL_ID), $3);
             }
           | relop_expr
             {
                 $$ = $1;
             }
           ;

relop_expr: relop_term
            {
                $$ = $1;
            }
          | relop_expr OP_OR relop_term
            {
                $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_OR), 2, $1, $3);
            }
          ;

relop_term: relop_factor
            {
                $$ = $1;
            }
          | relop_term OP_AND relop_factor
            {
                $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, BINARY_OP_AND), 2, $1, $3);
            }
          ;

relop_factor: expr
              {
                  $$ = $1;
              }
            | expr rel_op expr
              {
                  $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, $2), 2, $1, $3);
              }
            ;

rel_op: OP_EQ
        {
            $$ = BINARY_OP_EQ;
        }
      | OP_GE
        {
            $$ = BINARY_OP_GE;
        }
      | OP_LE
        {
            $$ = BINARY_OP_LE;
        }
      | OP_NE
        {
            $$ = BINARY_OP_NE;
        }
      | OP_GT
        {
            $$ = BINARY_OP_GT;
        }
      | OP_LT
        {
            $$ = BINARY_OP_LT;
        }
      ;

relop_expr_list: nonempty_relop_expr_list
                 {
                     $$ = MakeChild(Allocate(NONEMPTY_RELOP_EXPR_LIST_NODE), $1);
                 }
               | %empty
                 {
                     $$ = Allocate(NUL_NODE);
                 }
               ;

nonempty_relop_expr_list: nonempty_relop_expr_list MK_COMMA relop_expr
                          {
                              $$ = MakeSibling2($1, $3);
                          }
                        | relop_expr
                          {
                              $$ = $1;
                          }
                        ;

expr: expr add_op term
      {
          $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, $2), 2, $1, $3);
      }
    | term
      {
          $$ = $1;
      }
    ;

add_op: OP_PLUS
        {
            $$ = BINARY_OP_ADD;
        }
      | OP_MINUS
        {
            $$ = BINARY_OP_SUB;
        }
      ;

term: term mul_op factor
      {
          $$ = MakeFamily(MakeExprNode(BINARY_OPERATION, $2), 2, $1, $3);
      }
    | factor
      {
          $$ = $1;
      }
    ;

mul_op: OP_TIMES
        {
            $$ = BINARY_OP_MUL;
        }
      | OP_DIVIDE
        {
            $$ = BINARY_OP_DIV;
        }
      ;

factor: MK_LPAREN relop_expr MK_RPAREN
        {
            $$ = $2;
        }
      | OP_MINUS MK_LPAREN relop_expr MK_RPAREN
        {
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_NEGATIVE), $3);
        }
      | OP_NOT MK_LPAREN relop_expr MK_RPAREN
        {
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_LOGICAL_NEGATION), $3);
        }
      | CONST
        {
            auto *tmp = Allocate(CONST_VALUE_NODE);
            tmp->semantic_value = $1;
            $$ = tmp;
        }
      | OP_MINUS CONST
        {
            auto *tmp = Allocate(CONST_VALUE_NODE);
            tmp->semantic_value = $2;
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_NEGATIVE), tmp);
        }
      | OP_NOT CONST
        {
            auto *tmp = Allocate(CONST_VALUE_NODE);
            tmp->semantic_value = $2;
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_LOGICAL_NEGATION), tmp);
        }
      | ID MK_LPAREN relop_expr_list MK_RPAREN
        {
            auto *iden = MakeIdNode($1, NORMAL_ID);
            $$ = MakeFamily(MakeStmtNode(FUNCTION_CALL_STMT), 2, iden, $3);
        }
      | OP_MINUS ID MK_LPAREN relop_expr_list MK_RPAREN
        {
            auto *func = MakeFamily(MakeStmtNode(FUNCTION_CALL_STMT), 2, MakeIdNode($2, NORMAL_ID), $4);
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_NEGATIVE), func);
        }
      | OP_NOT ID MK_LPAREN relop_expr_list MK_RPAREN
        {
            auto *func = MakeFamily(MakeStmtNode(FUNCTION_CALL_STMT), 2, MakeIdNode($2, NORMAL_ID), $4);
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_LOGICAL_NEGATION), func);
        }
      | var_ref
        {
            $$ = $1;
        }
      | OP_MINUS var_ref
        {
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_NEGATIVE), $2);
        }
      | OP_NOT var_ref
        {
            $$ = MakeChild(MakeExprNode(UNARY_OPERATION, UNARY_OP_LOGICAL_NEGATION), $2);
        }
      ;

var_ref: ID
         {
             $$ = MakeIdNode($1, NORMAL_ID);
         }
       | ID dim_list
         {
             $$ = MakeChild(MakeIdNode($1, ARRAY_ID), $2);
         }
       ;

dim_list: dim_list MK_LBRACK expr MK_RBRACK 
          {
              $$ = MakeSibling2($1, $3);
          }
        | MK_LBRACK expr MK_RBRACK 
          {
              $$ = $2;
          }
        ;

%%

extern FILE *yyin;

int main(int argc, char **argv) {
  assert(argc > 1);
  yyin = fopen(argv[1], "r");
  yy::parser parse;
  parse();
  printf("%s\n", "Parsing completed. No errors found.");
  printGV(prog, nullptr);
}

void yy::parser::error(const std::string &m) { std::cerr << m << '\n'; }
