#include <stdlib.h>

#include "header.hh"

extern int line_number;

AstNode *Allocate(AstType type) {
  AstNode *temp;
  temp = new AstNode();
  temp->node_type = type;
  temp->data_type = NONE_TYPE;
  // Notice that leftmostSibling is not initialized as nullptr
  temp->leftmost_sibling = temp;
  temp->line_number = line_number;
  return temp;
}
