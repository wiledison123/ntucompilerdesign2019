%option noyywrap

%top {
  #pragma clang diagnostic ignored "-Wextra-semi-stmt"
  #pragma clang diagnostic ignored "-Wimplicit-fallthrough"
  #pragma clang diagnostic ignored "-Wold-style-cast"
  #pragma clang diagnostic ignored "-Wunreachable-code"
  #pragma clang diagnostic ignored "-Wzero-as-null-pointer-constant"
}

%{
  #include <cstdio>
  #include <string>

  #include "header.hh" 
  #include "parser.hh"

  #define YY_DECL yy::parser::symbol_type yylex(void)
  YY_DECL;

  extern int line_number;
%}

%x COMMENTMODE

letter [A-Za-z]
digit [0-9]
WS [ \t]+
NEWLINE "\n"

/* Reserved words */
ELSE "else"
FLOAT "float"
FOR "for"
IF "if"
INT "int"
RETURN "return"
TYPEDEF "typedef"
VOID "void"
WHILE "while"

/* Constants */
INT_CONSTANT {digit}+
FLOAT_EXPONENT (e|E)([+]|-)?{INT_CONSTANT}
FLOAT_DOTTED_DIGITS {INT_CONSTANT}[.]|[.]{INT_CONSTANT}|{INT_CONSTANT}[.]{INT_CONSTANT}
FLOAT_CONSTANT ({FLOAT_DOTTED_DIGITS}|{INT_CONSTANT}){FLOAT_EXPONENT}?[fFlL]?
STRING_CONSTANT ["][^"]*["]

/* Identifiers */
ID {letter}({letter}|{digit}|"_")*

/* Symbols and operators */
OP_PLUS "+"
OP_MINUS "-"
OP_TIMES "*"
OP_DIVIDE "/"

OP_LT "<"
OP_GT ">"
OP_LE "<="
OP_GE ">="
OP_NE "!="
OP_EQ "=="

OP_OR "||"
OP_AND "&&"
OP_NOT "!"

OP_ASSIGN "="

/* Separators */

MK_LBRACE "{"
MK_RBRACE "}"
MK_LBRACK "["
MK_RBRACK "]"
MK_LPAREN "("
MK_RPAREN ")"
MK_SEMICOLON ";"
MK_COMMA ","
MK_DOT "."

ERROR .

%%

{WS} {}
{NEWLINE} ++line_number;

"/*" BEGIN(COMMENTMODE);
<COMMENTMODE>[^*]* {}
<COMMENTMODE>[*]+[^*/]* {}
<COMMENTMODE>[*]+[/] BEGIN(INITIAL);

{ELSE} return yy::parser::make_ELSE();
{FLOAT} return yy::parser::make_FLOAT();
{FOR} return yy::parser::make_FOR();
{IF} return yy::parser::make_IF();
{INT} return yy::parser::make_INT();
{RETURN} return yy::parser::make_RETURN();
{TYPEDEF} return yy::parser::make_TYPEDEF();
{VOID} return yy::parser::make_VOID();
{WHILE} return yy::parser::make_WHILE();

{INT_CONSTANT} {
  return yy::parser::make_CONST(atoi(yytext));
}
{FLOAT_CONSTANT} {
  return yy::parser::make_CONST(atof(yytext));
}
{STRING_CONSTANT} {
  return yy::parser::make_CONST(std::string(yytext));
}

{ID} return yy::parser::make_ID(std::string(yytext));

{OP_PLUS} return yy::parser::make_OP_PLUS();
{OP_MINUS} return yy::parser::make_OP_MINUS();
{OP_TIMES} return yy::parser::make_OP_TIMES();
{OP_DIVIDE} return yy::parser::make_OP_DIVIDE();

{OP_LT} return yy::parser::make_OP_LT();
{OP_GT} return yy::parser::make_OP_GT();
{OP_LE} return yy::parser::make_OP_LE();
{OP_GE} return yy::parser::make_OP_GE();
{OP_NE} return yy::parser::make_OP_NE();
{OP_EQ} return yy::parser::make_OP_EQ();

{OP_OR} return yy::parser::make_OP_OR();
{OP_AND} return yy::parser::make_OP_AND();
{OP_NOT} return yy::parser::make_OP_NOT();

{OP_ASSIGN} return yy::parser::make_OP_ASSIGN();

{MK_LBRACE} return yy::parser::make_MK_LBRACE();
{MK_RBRACE} return yy::parser::make_MK_RBRACE();
{MK_LBRACK} return yy::parser::make_MK_LBRACK();
{MK_RBRACK} return yy::parser::make_MK_RBRACK();
{MK_LPAREN} return yy::parser::make_MK_LPAREN();
{MK_RPAREN} return yy::parser::make_MK_RPAREN();
{MK_SEMICOLON} return yy::parser::make_MK_SEMICOLON();
{MK_COMMA} return yy::parser::make_MK_COMMA();
{MK_DOT} return yy::parser::make_MK_DOT();

{ERROR} return yy::parser::make_ERROR();

%%

