#include <cstring>
#include <stack>
#include <unordered_set>

#include "symbol_table.hh"
#include "visitor.hh"

static const std::string kReturnLabelPrefix = ".RET_",
                         kGlobalVarPrefix = "", kConstValPrefix = ".L_FC_",
                         kGlobalStrPrefix = ".LC_", kIfEndPrefix = ".L_IF_END_",
                         kIfElsePrefix = ".L_IF_ELSE_",
                         kAndSkipPrefix = ".L_AND_",
                         kAndEndPrefix = ".L_AND_END_", kOrSkipPrefix = ".L_OR_",
                         kOrEndPrefix = ".L_OR_END_",
                         kWhileTestPrefix = ".L_WHILE_TEST_",
                         kWhileEndPrefix = ".L_WHILE_END_",
                         kForTestPrefix = ".L_FOR_TEST_",
                         kForEndPrefix = ".L_FOR_END_",
                         kFuncLabelPrefix = "_start_";

class PreGenerator {
 public:
	PreGenerator();

	void ProcessBinaryExprNode(AstNode *root);
	void ProcessUnaryExprNode(AstNode *root);
	void ProcessReturnStatementNode(AstNode *root);
	void ProcessFunctionCallStatementNode(AstNode *root);
	void ProcessIfStatementNode(AstNode *root);
	void ProcessAssignStatementNode(AstNode *root);
	void ProcessForStatementNode(AstNode *root);
	void ProcessWhileStatementNode(AstNode *root);
	void ProcessBlockBegin(AstNode *root, bool open_scope);
	void ProcessBlockEnd(AstNode *root, bool open_scope);
	void ProcessFunctionDeclBegin(AstNode *root);
	void ProcessFunctionDeclEnd(AstNode *root);
	void ProcessDeclBegin(AstNode *root);
	void ProcessNulNode(AstNode *root);
	void ProcessConstValueNode(AstNode *root);
	void ProcessIdentifierNode(AstNode *root, bool is_declare);
	void ProcessDeclEnd(AstNode *root);
	void ProcessNonEmptyRelopExprListBegin(AstNode *root);
	void ProcessNonEmptyRelopExprListEnd(AstNode *root);
	void ProcessDimInfoBegin(AstNode *root);
	void ProcessDimInfoEnd(AstNode *root);
	void ProcessVariableDeclListBegin(AstNode *root);
	void ProcessVariableDeclListEnd(AstNode *root);
	void ProcessNonEmptyAssignExprListBegin(AstNode *root);
	void ProcessNonEmptyAssignExprListEnd(AstNode *root);
	void ProcessStatementListBegin(AstNode *root);
	void ProcessStatementListEnd(AstNode *root);
	void ProcessNodeEnd(AstNode *root);
	void ProcessTypeDeclarationBegin(AstNode *root);
	void ProcessTypeDeclarationEnd(AstNode *root);

 private:
	SymbolTable symbol_table;
	std::stack<int> stack_offset_record;
	AstNode *current_function_astnode;
	int current_stack_offset;
	int maximum_offset;
	std::unordered_set<std::string> current_function_params;

	void SnapshotStackOffset();
	void RestoreStackOffset();
	int GetStackSpace(int x);
};

class Generator {
 public:
	void ProcessBinaryExprNode(AstNode *root);
	void ProcessUnaryExprNode(AstNode *root);
	void ProcessReturnStatementNode(AstNode *root);
	void ProcessFunctionCallStatementNode(AstNode *root);
	void ProcessIfStatementNode(AstNode *root);
	void ProcessAssignStatementNode(AstNode *root);
	void ProcessForStatementNode(AstNode *root);
	void ProcessWhileStatementNode(AstNode *root);
	void ProcessBlockBegin(AstNode *root, bool open_scope);
	void ProcessBlockEnd(AstNode *root, bool open_scope);
	void ProcessFunctionDeclBegin(AstNode *root);
	void ProcessFunctionDeclEnd(AstNode *root);
	void ProcessDeclBegin(AstNode *root);
	void ProcessDeclEnd(AstNode *root);
	void ProcessNulNode(AstNode *root);
	void ProcessConstValueNode(AstNode *root);
	void ProcessIdentifierNode(AstNode *root, bool is_declare);
	void ProcessNodeEnd(AstNode *root);
	void ProcessVariableDeclListBegin(AstNode *root);
	void ProcessVariableDeclListEnd(AstNode *root);
	void ProcessNonEmptyRelopExprListBegin(AstNode *root);
	void ProcessNonEmptyRelopExprListEnd(AstNode *root);
	void ProcessNonEmptyAssignExprListBegin(AstNode *root);
	void ProcessNonEmptyAssignExprListEnd(AstNode *root);
	void ProcessStatementListBegin(AstNode *root);
	void ProcessStatementListEnd(AstNode *root);
	void ProcessTypeDeclarationBegin(AstNode *root);
	void ProcessTypeDeclarationEnd(AstNode *root);

 private:
	AstNode *current_function_node;
	std::stringstream global_data;

	void ResolveDimAddress(AstNode *now, std::stringstream &ss,
	                       std::string dest);
	void ResolveArrayDimensions(AstNode *root);
	void ResolveAddress(AstNode *now, std::stringstream &ss, std::string dest);
	void ProcessTestNode(AstNode *root, std::stringstream &ss, std::string dest);
	void AssignImpl(AstNode *dst, AstNode *src, std::stringstream &o);
	void ConsumeChildCode(AstNode *root);
	void GenerateAddi(std::stringstream &ss, std::string rd, std::string ra,
	                  int imm);
	void GenerateSd(std::stringstream &ss, std::string rd, int imm,
	                std::string ba);
	void GenerateLd(std::stringstream &ss, std::string rd, int imm,
	                std::string ba);
	void GenerateSw(std::stringstream &ss, std::string rd, int imm,
	                std::string ba);
	void GenerateFsw(std::stringstream &ss, std::string rd, int imm,
	                std::string ba);
};
