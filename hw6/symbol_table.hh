#ifndef SYMBOL_TABLE_HH_
#define SYMBOL_TABLE_HH_

#include <cassert>
#include <deque>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

#include "header.hh"

class SymbolTable {
 public:
	using Offset_t = int;
	using HashTable = std::unordered_map<std::string, std::pair<SymbolType, int>>;

	SymbolTable() : stack() {
		HashTable init;
		init["int"] = {TypedefType{{INT_TYPE, {}}}, 0};
		init["float"] = {TypedefType{{FLOAT_TYPE, {}}}, 0};
		init["void"] = {TypedefType{{VOID_TYPE, {}}}, 0};
		init["read"] = {FunctionType{INT_TYPE, {}}, 0};
		init["fread"] = {FunctionType{FLOAT_TYPE, {}}, 0};
		stack.emplace_back(std::move(init));
	}

	void InsertVariable(AstNode *now, SymbolType datatype,
	                    const std::string &name, Offset_t ofst = 0);

	void InsertType(AstNode *now, ScalarArrayType target,
	                const std::string &name);

	SymbolType GetVariableType(AstNode *now, const std::string &name);

	ScalarArrayType GetTypedef(AstNode *now, const std::string &name);

	ScalarArrayType CheckCallable(AstNode *now, const std::string &name,
	                              const std::vector<ScalarArrayType> &types);

	void OpenScope();

	void CloseScope();

	Offset_t GetOffset(AstNode *now, const std::string &name);

	int GetScopeLevel(AstNode *now, const std::string &name);

 private:
	std::deque<HashTable> stack;  // Deque for random access
};

#endif
