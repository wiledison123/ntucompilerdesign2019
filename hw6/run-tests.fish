#!/bin/fish
echo -n > tmp
for f in pattern/extensive/*.c;
	echo "====== $f ======"
	echo (basename $f) >> tmp
	./parser $f 2>/dev/null >&2; riscv64-linux-gnu-gcc -Wall -static -ggdb3 main.S -o r.out; qemu-riscv64 ./r.out >> tmp
	echo "" >> tmp
end
diff tmp pattern/extensive/output
for f in pattern/extensive/*.c;
	echo "====== $f ======"
	diff (./parser (sed -e 's/main/MAIN/' $f | psub) 2>/dev/null >&2; riscv64-linux-gnu-gcc -Wall -static -ggdb3 main.S -o r.out; qemu-riscv64 ./r.out | psub) (./test-with-gcc.sh $f 2>/dev/null >&2; ./a.out | psub)
end
for f in pattern/without_errors/*.c;
	echo "====== $f ======"
	diff (./parser (sed -e 's/main/MAIN/' $f | psub) 2>/dev/null >&2; riscv64-linux-gnu-gcc -Wall -static -ggdb3 main.S -o r.out; qemu-riscv64 ./r.out | psub) (./test-with-gcc.sh $f 2>/dev/null >&2; ./a.out | psub)
end
for f in pattern/*.c;
	echo "====== $f ======"
	diff (./parser $f 2>/dev/null >&2; riscv64-linux-gnu-gcc -Wall -static -ggdb3 main.S -o r.out; qemu-riscv64 ./r.out | psub) (./test-with-gcc.sh $f 2>/dev/null >&2; ./a.out | psub)
end
for f in pattern/has_output/*.c;
	echo "====== $f ======"
	diff (./parser $f 2>/dev/null >&2; riscv64-linux-gnu-gcc -Wall -static -ggdb3 main.S -o r.out; qemu-riscv64 ./r.out | psub) (cat (echo "$f" | sed 's/c$/output/') | psub)
end
