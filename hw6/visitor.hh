#ifndef VISITOR_HH_
#define VISITOR_HH_
#include <cassert>

#include "header.hh"
#include "printer.hh"

template <class Client>
class AstVisitor {
 public:
	AstVisitor(Client c) : client_(std::move(c)) {}

	void Visit(AstNode * const root) {
		assert(root->nodeType == PROGRAM_NODE);
#ifdef DEBUG
		PrintAst(std::cerr, 0, root);
#endif
		auto *node = root->child;
		while (node) {
			if (node->nodeType == DECLARATION_NODE)
				ProcessFunctionDeclarationNode(node);
			else if (node->nodeType == VARIABLE_DECL_LIST_NODE)
				ProcessVariableDeclarationListNode(node);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
	}

 private:
	Client client_;

	/* ===== Nodes that calls the client ===== */

	void ProcessExprNode(AstNode * const root) {
		assert(root->nodeType == EXPR_NODE);
		auto value = std::get<ExprSemanticVal>(root->semantic_value);
		if (value.kind == BINARY_OPERATION) {
			AstNode *c1 = root->child, *c2 = root->child->rightSibling;
			ProcessNode(c1);
			ProcessNode(c2);
			client_.ProcessBinaryExprNode(root);
		} else {
			ProcessNode(root->child);
			client_.ProcessUnaryExprNode(root);
		}
		if (root->dataType.datatype == ERROR_TYPE) DeleteAstNode(root->child);
		client_.ProcessNodeEnd(root);
	}

	void ProcessReturnStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == RETURN_STMT);
		auto *node = root->child;
		ProcessNode(node);
		client_.ProcessReturnStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessFunctionCallStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind ==
		           FUNCTION_CALL_STMT);
		auto *node = root->child;   // Identifier name
		node = node->rightSibling;  // Parameters
		if (node->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE)
			ProcessNonEmptyRelopExprListNode(node);
		client_.ProcessFunctionCallStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessIfStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == IF_STMT);
		auto *node = root->child;
		ProcessNode(node);
		ProcessNode(node->rightSibling);
		ProcessNode(node->rightSibling->rightSibling);
		client_.ProcessIfStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessAssignStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == ASSIGN_STMT);
		auto *node = root->child;
		ProcessNode(node);
		ProcessNode(node->rightSibling);
		client_.ProcessAssignStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessForStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == FOR_STMT);
		auto *node = root->child;
		ProcessNode(node);  // Init
		node = node->rightSibling;
		ProcessNode(node);  // Condition
		node = node->rightSibling;
		ProcessNode(node);  // Increment
		node = node->rightSibling;
		ProcessNode(node);  // Block
		client_.ProcessForStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessWhileStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE &&
		       std::get<StmtSemanticVal>(root->semantic_value).kind == WHILE_STMT);
		auto *node = root->child;
		ProcessNode(node);
		ProcessNode(node->rightSibling);
		client_.ProcessWhileStatementNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessBlockNode(AstNode * const root, bool open_scope) {
		assert(root->nodeType == BLOCK_NODE);
		client_.ProcessBlockBegin(root, open_scope);
		auto *node = root->child;
		while (node) {
			if (node->nodeType == VARIABLE_DECL_LIST_NODE)
				ProcessVariableDeclarationListNode(node);
			else if (node->nodeType == STMT_LIST_NODE)
				ProcessStatementListNode(node);
			else
				assert(0);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessBlockEnd(root, open_scope);
	}

	void ProcessFunctionDeclarationNode(AstNode * const root) {
		assert(root->nodeType == DECLARATION_NODE);
		client_.ProcessFunctionDeclBegin(root);
		auto *node = root->child;   // Type
		node = node->rightSibling;  // Name
		node = node->rightSibling;  // Params
		node = node->rightSibling;  // Block
		ProcessBlockNode(node, false);
		client_.ProcessNodeEnd(root);
		client_.ProcessFunctionDeclEnd(root);
	}

	void ProcessDeclarationNode(AstNode * const root) {
		assert(root->nodeType == DECLARATION_NODE &&
		       std::get<DeclSemanticVal>(root->semantic_value).kind ==
		           VARIABLE_DECL);
		auto *node = root->child;
		while ((node = node->rightSibling)) {
			client_.ProcessDeclBegin(node);
			if (int knd = std::get<IdSemanticVal>(node->semantic_value).kind;
			    knd == WITH_INIT_ID) {
				ProcessIdentifierNode(node, true);
				ProcessNode(node->child);
			} else if (knd == ARRAY_ID) {
				auto *dim_node = node->child;
				while (dim_node) {
					ProcessNode(dim_node);
					dim_node = dim_node->rightSibling;
				}
			}
			client_.ProcessDeclEnd(node);
		}
		client_.ProcessNodeEnd(root);
	}

	void ProcessTypeDeclarationNode(AstNode * const root) {
		assert(root->nodeType == DECLARATION_NODE &&
		       std::get<DeclSemanticVal>(root->semantic_value).kind == TYPE_DECL);
		// client_.ProcessTypeDeclarationNode(root);
		auto *node = root->child;
		while ((node = node->rightSibling)) {
			client_.ProcessTypeDeclarationBegin(node);
			if (std::get<IdSemanticVal>(node->semantic_value).kind == ARRAY_ID) {
				auto *dim_node = node->child;
				while (dim_node) {
					ProcessNode(dim_node);
					dim_node = dim_node->rightSibling;
				}
			}
			client_.ProcessTypeDeclarationEnd(node);
		}
		client_.ProcessNodeEnd(root);
	}

	void ProcessNonEmptyRelopExprListNode(AstNode * const root) {
		assert(root->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE);
		client_.ProcessNonEmptyRelopExprListBegin(root);
		auto *node = root->child;
		while (node) {
			ProcessNode(node);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessNonEmptyRelopExprListEnd(root);
		// MEMO: The client needs to do its own traversing to collect the list.
		//client_.ProcessNonEmptyRelopExprListNode(root);
	}

	__attribute__((deprecated))
	void ProcessDimInfo(AstNode * const root) {
		auto *node = root;
		client_.ProcessDimInfoBegin(root);
		while (node) {
			ProcessNode(node);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessDimInfoEnd(root);
	}

	void ProcessVariableDeclarationListNode(AstNode * const root) {
		assert(root->nodeType == VARIABLE_DECL_LIST_NODE);
		client_.ProcessVariableDeclListBegin(root);
		auto *node = root->child;
		while (node) {
			if (std::get<DeclSemanticVal>(node->semantic_value).kind == VARIABLE_DECL)
				ProcessDeclarationNode(node);
			else
				ProcessTypeDeclarationNode(node);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessVariableDeclListEnd(root);
	}

	void ProcessNonEmptyAssignExprListNode(AstNode * const root) {
		assert(root->nodeType == NONEMPTY_ASSIGN_EXPR_LIST_NODE);
		client_.ProcessNonEmptyAssignExprListBegin(root);
		auto *node = root->child;
		while (node) {
			assert(node->nodeType == STMT_NODE &&
			       std::get<StmtSemanticVal>(node->semantic_value).kind ==
			           ASSIGN_STMT);
			ProcessNode(node);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessNonEmptyAssignExprListEnd(root);
	}

	void ProcessStatementListNode(AstNode * const root) {
		assert(root->nodeType == STMT_LIST_NODE);
		client_.ProcessStatementListBegin(root);
		auto *node = root->child;
		while (node) {
			if (node->nodeType == BLOCK_NODE)
				ProcessBlockNode(node, true);
			else if (node->nodeType == STMT_NODE)
				ProcessStatementNode(node);
			else if (node->nodeType == NUL_NODE)
				;
			else
				assert(0);
			node = node->rightSibling;
		}
		client_.ProcessNodeEnd(root);
		client_.ProcessStatementListEnd(root);
	}

	void ProcessIdentifierNode(AstNode * const root, bool is_declare) {
		if (std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID) {
			auto *node = root->child;
			while (node) {
				ProcessNode(node);
				node = node->rightSibling;
			}
		}
		client_.ProcessIdentifierNode(root, is_declare);
		client_.ProcessNodeEnd(root);
	}

	/* ===== Leaf nodes ===== */

	void ProcessNulNode(AstNode * const root) {
		client_.ProcessNulNode(root);
		client_.ProcessNodeEnd(root);
	}

	void ProcessConstValueNode(AstNode * const root) {
		client_.ProcessConstValueNode(root);
		client_.ProcessNodeEnd(root);
	}

	/* ===== Helper / dispatcher functions ===== */

	void DeleteAstNode(AstNode *&z) {
		if (z->child) DeleteAstNode(z->child);
		if (z->rightSibling) DeleteAstNode(z->rightSibling);
		delete z;
		z = nullptr;
	}

	void ProcessNode(AstNode * const root) {
		switch (root->nodeType) {
			case DECLARATION_NODE:
				ProcessDeclarationNode(root);
				break;
			case IDENTIFIER_NODE:
				ProcessIdentifierNode(root, false);
				break;
			case NUL_NODE:
				ProcessNulNode(root);
				break;
			case BLOCK_NODE:
				ProcessBlockNode(root, true);
				break;
			case STMT_NODE:
				ProcessStatementNode(root);
				break;
			case STMT_LIST_NODE:
				ProcessStatementListNode(root);
				break;
			case EXPR_NODE:
				ProcessExprNode(root);
				break;
			case CONST_VALUE_NODE:
				ProcessConstValueNode(root);
				break;
			case NONEMPTY_ASSIGN_EXPR_LIST_NODE:
				ProcessNonEmptyAssignExprListNode(root);
				break;
			// case NONEMPTY_RELOP_EXPR_LIST_NODE:
			// ProcessNonEmptyRelopExprListNode(root); break;
			case NULL_ASTTYPE:
			case PROGRAM_NODE:
			case PARAM_LIST_NODE:
			case VARIABLE_DECL_LIST_NODE:
			case NONEMPTY_RELOP_EXPR_LIST_NODE:
				PrintError(root, "This is a bug: Unsupported node type: ",
				           AstType2str[root->nodeType]);
		}
	}

	void ProcessStatementNode(AstNode * const root) {
		assert(root->nodeType == STMT_NODE);
		int knd = std::get<StmtSemanticVal>(root->semantic_value).kind;
		switch (knd) {
			case WHILE_STMT:
				ProcessWhileStatementNode(root);
				break;
			case FOR_STMT:
				ProcessForStatementNode(root);
				break;
			case ASSIGN_STMT:
				ProcessAssignStatementNode(root);
				break;
			case IF_STMT:
				ProcessIfStatementNode(root);
				break;
			case FUNCTION_CALL_STMT:
				ProcessFunctionCallStatementNode(root);
				break;
			case RETURN_STMT:
				ProcessReturnStatementNode(root);
				break;
		}
	}
};
#endif  // VISITOR_HH_
