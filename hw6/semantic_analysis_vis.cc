#include <cassert>
#include <cstdio>
#include <vector>

#include "printer.hh"
#include "semantic_analysis_vis.hh"
#include "symbol_table.hh"
#include "visitor.hh"

void SemanticAnalyzer::ProcessBinaryExprNode(AstNode *root) {
	auto value = std::get<ExprSemanticVal>(root->semantic_value);
	AstNode *c1 = root->child, *c2 = root->child->rightSibling;
	DataType type1 = c1->dataType.datatype;
	DataType type2 = c2->dataType.datatype;
	if (c1->dataType.dims.size() || c2->dataType.dims.size()) {
		PrintError(root, "Arithmetic of array types.");
		root->dataType = ERROR_TYPE;
	} else if (type1 == ERROR_TYPE || type2 == ERROR_TYPE) {
		root->dataType = ERROR_TYPE;
	} else {
		if ((type1 != INT_TYPE && type1 != FLOAT_TYPE) ||
		    (type2 != INT_TYPE && type2 != FLOAT_TYPE)) {
			root->dataType = ERROR_TYPE;
			PrintError(root, "Error type to perform operator ",
			           Operator2str[value.op]);
		} else {
			if ((type1 != INT_TYPE && type1 != FLOAT_TYPE) ||
			    (type2 != INT_TYPE && type2 != FLOAT_TYPE)) {
				root->dataType = ERROR_TYPE;
				PrintError(root, "Error type to perform operator ",
				           Operator2str[value.op]);
			} else {
				DataType type =
				    (type1 == INT_TYPE && type2 == INT_TYPE ? INT_TYPE : FLOAT_TYPE);
				if (type == FLOAT_TYPE &&
				    (value.op == BINARY_OP_AND || value.op == BINARY_OP_OR))
					PrintError(root, "Can not apply " + Operator2str[value.op] + " to " +
					                     DataType2str[type1] + " and " +
					                     DataType2str[type2]);
				if (value.op == BINARY_OP_EQ || value.op == BINARY_OP_GE ||
				    value.op == BINARY_OP_LE || value.op == BINARY_OP_NE ||
				    value.op == BINARY_OP_GT || value.op == BINARY_OP_LT ||
				    value.op == BINARY_OP_AND || value.op == BINARY_OP_OR)
					type = INT_TYPE;
				root->dataType = {type, {}};
				if (c1->nodeType == CONST_VALUE_NODE &&
				    c2->nodeType == CONST_VALUE_NODE)
					ConstFold(root);
			}
		}
	}
}

void SemanticAnalyzer::ProcessUnaryExprNode(AstNode *root) {
	auto value = std::get<ExprSemanticVal>(root->semantic_value);
	if (root->child->dataType.dims.size()) {
		PrintError(root, "Arithmetic of array types.");
		root->dataType = ERROR_TYPE;
	}
	if (root->child->dataType.datatype == ERROR_TYPE) {
		root->dataType = ERROR_TYPE;
	} else {
		DataType type = root->child->dataType.datatype;
		if (type != INT_TYPE && type != FLOAT_TYPE) {
			root->dataType = ERROR_TYPE;
			PrintError(root, "Error type to perform operator ",
			           Operator2str[value.op]);
		} else {
			type = root->child->dataType.datatype;
			if (type != INT_TYPE && type != FLOAT_TYPE) {
				root->dataType = ERROR_TYPE;
				PrintError(root, "Error type to perform operator ",
				           Operator2str[value.op]);
			} else {
				root->dataType = {root->child->dataType.datatype, {}};
				if (value.op == UNARY_OP_LOGICAL_NEGATION)
					root->dataType.datatype = INT_TYPE;
				if (root->child->nodeType == CONST_VALUE_NODE) ConstFold(root);
			}
		}
	}
}

void SemanticAnalyzer::ProcessReturnStatementNode(AstNode *root) {
	root = root->child;
	CheckAssignable(root, current_function_return_type, root->dataType, true);
}

void SemanticAnalyzer::ProcessFunctionCallStatementNode(AstNode *root) {
	auto func_root = root;
	root = root->child;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root = root->rightSibling;
	std::vector<ScalarArrayType> types;
	if (root->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE)
		types = ProcessNonEmptyRelopExprListNode(root);
	func_root->dataType = symbol_table.CheckCallable(root, name, types);
}

void SemanticAnalyzer::ProcessIfStatementNode(AstNode *root) {
	root = root->child;
	if (!CheckTruthable(root->dataType)) {
		PrintError(root,
		           "If can not test type: ", DataType2str[root->dataType.datatype]);
	}
}

void SemanticAnalyzer::ProcessAssignStatementNode(AstNode *root) {
	root = root->child;
	CheckAssignable(root, root->dataType, root->rightSibling->dataType);
}

void SemanticAnalyzer::ProcessForStatementNode(AstNode *root) {
	root = root->child->rightSibling;  // Condition
	if (!CheckTruthable(root->dataType))
		PrintError(
		    root, "For can not test type: ", DataType2str[root->dataType.datatype]);
}

void SemanticAnalyzer::ProcessWhileStatementNode(AstNode *root) {
	root = root->child;
	if (!CheckTruthable(root->dataType)) {
		PrintError(root, "While can not test type: ",
		           DataType2str[root->dataType.datatype]);
	}
}

void SemanticAnalyzer::ProcessBlockBegin(AstNode *root, bool open_scope) {
	(void)root;
	if (open_scope) symbol_table.OpenScope();
}

void SemanticAnalyzer::ProcessBlockEnd(AstNode *root, bool open_scope) {
	(void)root;
	if (open_scope) symbol_table.CloseScope();
}

void SemanticAnalyzer::ProcessFunctionDeclBegin(AstNode *root) {
	root = root->child;
	std::string type =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(root, type);
	if (real_type.dims.size()) {
		PrintError(root, "Return value has to be a scalar.");
	}
	current_function_return_type = real_type;
	root->dataType = real_type;
	root = root->rightSibling;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root = root->rightSibling;
	auto param = CollectParamDeclarationInfo(root);
	std::vector<ScalarArrayType> param_type;
	for (auto &par : param) param_type.push_back(par.first);
	symbol_table.InsertVariable(
	    root, FunctionType{real_type.datatype, param_type}, name);
	symbol_table.OpenScope();
	for (auto &par : param)
		symbol_table.InsertVariable(root, par.first, par.second);
}

void SemanticAnalyzer::ProcessFunctionDeclEnd(AstNode *root) {
	(void)root;
	symbol_table.CloseScope();
	current_function_return_type = NONE_TYPE;
}

void SemanticAnalyzer::ProcessDeclBegin(AstNode *root) {
	auto *type_node = root->leftmostSibling;
	std::string type =
	    std::get<IdSemanticVal>(type_node->semantic_value).identifierName;
	auto cur_type = symbol_table.GetTypedef(type_node, type);
	root->dataType = cur_type;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	// Arrays are not inserted into the table yet.
	// This is because the dimensions needs to be parsed.
	// Since arrays cannot be initialized in our C-- variant,
	// inserting it at ProcessDeclEnd is okay.
	if (int knd = std::get<IdSemanticVal>(root->semantic_value).kind;
	    knd == WITH_INIT_ID || knd == NORMAL_ID) {
		symbol_table.InsertVariable(root, cur_type, name);
	}
}

void SemanticAnalyzer::ProcessDeclEnd(AstNode *root) {
	auto *type_node = root->leftmostSibling;
	std::string type =
	    std::get<IdSemanticVal>(type_node->semantic_value).identifierName;
	auto cur_type = symbol_table.GetTypedef(type_node, type);
	root->dataType = cur_type;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	if (int knd = std::get<IdSemanticVal>(root->semantic_value).kind;
	    knd == WITH_INIT_ID) {
		if (current_function_return_type.datatype == NONE_TYPE) 
			PrintError(root, "Initializer element is not constant\n");
		CheckAssignable(root, root->dataType, root->child->dataType);
	} else if (knd == ARRAY_ID) {
		if (cur_type.datatype == VOID_TYPE) 
			PrintError(root, "Declare " + name + " as array of voids");
		std::vector<int> dims = CollectDimInfo(root->child);
		cur_type.dims.insert(cur_type.dims.begin(), dims.begin(), dims.end());
		root->dataType = cur_type;
		symbol_table.InsertVariable(root, cur_type, name);
	}
}

void SemanticAnalyzer::ProcessTypeDeclarationBegin(AstNode *root) {
	auto *type_node = root->leftmostSibling;
	std::string type = 
		std::get<IdSemanticVal>(type_node->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(type_node, type);
	std::string name = 
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	if (std::get<IdSemanticVal>(root->semantic_value).kind == NORMAL_ID) 
		symbol_table.InsertType(root, real_type, name);
}

void SemanticAnalyzer::ProcessTypeDeclarationEnd(AstNode *root) {
	auto *type_node = root->leftmostSibling;
	std::string type = 
		std::get<IdSemanticVal>(type_node->semantic_value).identifierName;
	auto real_type = symbol_table.GetTypedef(type_node, type);
	std::string name = 
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	if (std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID) {
		if (real_type.datatype == VOID_TYPE) 
			PrintError(root, "Declaration of " + name + " as array of void");
		std::vector<int> dims = CollectDimInfo(root->child);
		real_type.dims.insert(real_type.dims.begin(), dims.begin(), dims.end());
		symbol_table.InsertType(root, real_type, name);
	}
}

void SemanticAnalyzer::ProcessNonEmptyRelopExprListBegin(AstNode *root) {
	(void)root;
}
void SemanticAnalyzer::ProcessNonEmptyRelopExprListEnd(AstNode *root) {
	(void)root;
}
void SemanticAnalyzer::ProcessDimInfoBegin(AstNode *root) { (void)root; }
void SemanticAnalyzer::ProcessDimInfoEnd(AstNode *root) { (void)root; }
void SemanticAnalyzer::ProcessVariableDeclListBegin(AstNode *root) {
	(void)root;
}
void SemanticAnalyzer::ProcessVariableDeclListEnd(AstNode *root) { (void)root; }
void SemanticAnalyzer::ProcessNonEmptyAssignExprListBegin(AstNode *root) {
	(void)root;
}
void SemanticAnalyzer::ProcessNonEmptyAssignExprListEnd(AstNode *root) {
	(void)root;
}
void SemanticAnalyzer::ProcessStatementListBegin(AstNode *root) { (void)root; }
void SemanticAnalyzer::ProcessStatementListEnd(AstNode *root) { (void)root; }

void SemanticAnalyzer::ProcessNulNode(AstNode *root) {
	root->dataType = VOID_TYPE;
}

void SemanticAnalyzer::ProcessConstValueNode(AstNode *root) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
	root->dataType =
	    std::visit(overload{[](double x) { return FLOAT_TYPE; },
	                        [](int x) { return INT_TYPE; },
	                        [](std::string x) { return CONST_STRING_TYPE; }},
	               std::get<Constant>(root->semantic_value));
#pragma GCC diagnostic pop
}

void SemanticAnalyzer::ProcessIdentifierNode(AstNode *root, bool is_declare) {
	(void)is_declare;
	std::string name =
	    std::get<IdSemanticVal>(root->semantic_value).identifierName;
	auto type =
	    std::get<ScalarArrayType>(symbol_table.GetVariableType(root, name));
	if (type.datatype == ERROR_TYPE)
		PrintError(root, name, " was not declared here.");
	else if (std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID) {
		AstNode *child = root->child;
		while (child) {
			// TODO-IMPROVE: This is in quadratic complexity.
			if (type.dims.size()) {
				type.dims.erase(type.dims.begin());
				child = child->rightSibling;
			} else {
				PrintError(root, "Attempt to index into a scalar.");
				break;
			}
		}
	}
	root->dataType = type;
}

std::vector<ScalarArrayType> SemanticAnalyzer::ProcessNonEmptyRelopExprListNode(
    AstNode *root) {
	root = root->child;
	std::vector<ScalarArrayType> res;
	while (root) {
		res.push_back(root->dataType);
		root = root->rightSibling;
	}
	return res;
}

void SemanticAnalyzer::ProcessNodeEnd(AstNode *root) { (void)root; }

bool SemanticAnalyzer::CheckTruthable(const ScalarArrayType &type) {
	auto t = type.datatype;
	return type.dims.empty() && !(t == NULL_DATATYPE || t == VOID_TYPE ||
	                              t == NONE_TYPE || t == ERROR_TYPE);
}

void SemanticAnalyzer::CheckAssignable(AstNode *now, const ScalarArrayType &lhs,
                                       const ScalarArrayType &rhs,
                                       bool check_return) {
	if (lhs.dims.size() || rhs.dims.size()) {
		PrintError(now, "Arrays not re-assignable.");
	}
	auto l = lhs.datatype, r = rhs.datatype;
	if (l == INT_TYPE && r == INT_TYPE)
		;
	else if (l == FLOAT_TYPE && r == INT_TYPE)
		;
	else if (l == INT_TYPE && r == FLOAT_TYPE)
		;
	else if (l == FLOAT_TYPE && r == FLOAT_TYPE)
		;
	else if (l == VOID_TYPE && r == VOID_TYPE)
		;
	else {
		if (check_return)
			PrintWarning(now, "Type error to return: returning ", DataType2str[r],
			             " in function which returns ", DataType2str[l]);
		else
			PrintError(now, "Type error to assign: assigning ", DataType2str[r],
			           " to ", DataType2str[l]);
	}
}

std::vector<int> SemanticAnalyzer::CollectDimInfo(AstNode *root) {
	std::vector<int> dims;
	bool is_first = true;
	while (root) {
		if (is_first && root->nodeType == NUL_NODE) {
			dims.push_back(-1);
		} else {
			if (root->nodeType == NUL_NODE) {
				PrintError(root, "Only the first dimension may be omitted.");
				return std::vector<int>();
			}
			if (!std::holds_alternative<Constant>(root->semantic_value)) {
				PrintError(root, "Value in dimensions is not constant.");
				return std::vector<int>();
			}
			auto &cnst = std::get<Constant>(root->semantic_value);
			if (!std::holds_alternative<int>(cnst)) {
				PrintError(root, "Value in dimensions should have type int.");
				return std::vector<int>();
			}
			if (std::get<int>(cnst) <= 0) {
				PrintError(root, "Value in dimensions should be greater than 0.");
				return std::vector<int>();
			}
			dims.push_back(std::get<int>(cnst));
		}
		is_first = false;
		root = root->rightSibling;
	}
	return dims;
}

std::vector<std::pair<ScalarArrayType, std::string>>
SemanticAnalyzer::CollectParamDeclarationInfo(AstNode *root) {
	std::vector<std::pair<ScalarArrayType, std::string>> result;
	root = root->child;
	while (root) {
		AstNode *now = root->child;
		std::string type =
		    std::get<IdSemanticVal>(now->semantic_value).identifierName;
		auto real_type = symbol_table.GetTypedef(root, type);
		now = now->rightSibling;
		std::string name =
		    std::get<IdSemanticVal>(now->semantic_value).identifierName;
		if (int knd = std::get<IdSemanticVal>(now->semantic_value).kind;
		    knd == NORMAL_ID) {
			result.emplace_back(real_type, name);
		} else if (knd == ARRAY_ID) {
			now = now->child;
			std::vector<int> dims = CollectDimInfo(now);
			if (dims.size()) {
				real_type.dims.insert(real_type.dims.begin(), dims.begin(), dims.end());
				result.emplace_back(real_type, name);
			} else {
				// Error, but we push something in anyway.
				result.emplace_back(real_type, name);
			}
		}
		root->dataType = real_type;
		root = root->rightSibling;
	}
	return result;
}

void SemanticAnalyzer::ConstFold(AstNode *root) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wfloat-equal"
	root->nodeType = CONST_VALUE_NODE;
	auto value = std::get<ExprSemanticVal>(root->semantic_value);
	if (value.kind == BINARY_OPERATION) {
		std::visit(overload{[&](auto x, auto y) -> void {
			                    switch (value.op) {
				                    case BINARY_OP_ADD:
					                    root->semantic_value = x + y;
					                    break;
				                    case BINARY_OP_SUB:
					                    root->semantic_value = x - y;
					                    break;
				                    case BINARY_OP_MUL:
					                    root->semantic_value = x * y;
					                    break;
				                    case BINARY_OP_DIV:
					                    if (y != 0) root->semantic_value = x / y;
					                    break;
				                    case BINARY_OP_EQ:
					                    root->semantic_value = x == y;
					                    break;
				                    case BINARY_OP_GE:
					                    root->semantic_value = x >= y;
					                    break;
				                    case BINARY_OP_LE:
					                    root->semantic_value = x <= y;
					                    break;
				                    case BINARY_OP_NE:
					                    root->semantic_value = x != y;
					                    break;
				                    case BINARY_OP_GT:
					                    root->semantic_value = x > y;
					                    break;
				                    case BINARY_OP_LT:
					                    root->semantic_value = x < y;
					                    break;
				                    case BINARY_OP_AND:
					                    root->semantic_value =
					                        static_cast<bool>(x) && static_cast<bool>(y);
					                    break;
				                    case BINARY_OP_OR:
					                    root->semantic_value =
					                        static_cast<bool>(x) || static_cast<bool>(y);
					                    break;
				                    case UNARY_OP_POSITIVE:
				                    case UNARY_OP_NEGATIVE:
				                    case UNARY_OP_LOGICAL_NEGATION:
					                    assert(0 && "Unary operator not expected.");
			                    }
		                    },
		                    [](int x, std::string y) -> void {},
		                    [](double x, std::string y) -> void {},
		                    [](std::string x, int y) -> void {},
		                    [](std::string x, double y) -> void {},
		                    [](std::string x, std::string y) -> void {}},
		           std::get<Constant>(root->child->semantic_value),
		           std::get<Constant>(root->child->rightSibling->semantic_value));
	} else {
		std::visit(overload{[&](auto x) -> void {
			                    switch (value.op) {
				                    case UNARY_OP_POSITIVE:
					                    root->semantic_value = x;
					                    break;
				                    case UNARY_OP_NEGATIVE:
					                    root->semantic_value = -x;
					                    break;
				                    case UNARY_OP_LOGICAL_NEGATION:
					                    root->dataType = INT_TYPE;
					                    root->semantic_value = !static_cast<bool>(x);
					                    break;
				                    case BINARY_OP_ADD:
				                    case BINARY_OP_SUB:
				                    case BINARY_OP_MUL:
				                    case BINARY_OP_DIV:
				                    case BINARY_OP_EQ:
				                    case BINARY_OP_GE:
				                    case BINARY_OP_LE:
				                    case BINARY_OP_NE:
				                    case BINARY_OP_GT:
				                    case BINARY_OP_LT:
				                    case BINARY_OP_AND:
				                    case BINARY_OP_OR:
					                    assert(0 && "Binary operator not expected.");
			                    }
		                    },
		                    [](std::string x) -> void {}},
		           std::get<Constant>(root->child->semantic_value));
	}
	// DeleteAstNode(root->child);
#pragma GCC diagnostic pop
}
