#ifndef SEMANTIC_ANALYSIS_VIS_HH_
#define SEMANTIC_ANALYSIS_VIS_HH_

#include <cassert>
#include <cstdio>
#include <vector>

#include "printer.hh"
#include "symbol_table.hh"
#include "visitor.hh"

class SemanticAnalyzer {
 public:
	void ProcessBinaryExprNode(AstNode *root);

	void ProcessUnaryExprNode(AstNode *root);

	void ProcessReturnStatementNode(AstNode *root);

	void ProcessFunctionCallStatementNode(AstNode *root);

	void ProcessIfStatementNode(AstNode *root);

	void ProcessAssignStatementNode(AstNode *root);

	void ProcessForStatementNode(AstNode *root);

	void ProcessWhileStatementNode(AstNode *root);

	void ProcessBlockBegin(AstNode *root, bool open_scope);

	void ProcessBlockEnd(AstNode *root, bool open_scope);

	void ProcessFunctionDeclBegin(AstNode *root);

	void ProcessFunctionDeclEnd(AstNode *root);

	void ProcessDeclBegin(AstNode *root);

	void ProcessDeclEnd(AstNode *root);

	void ProcessTypeDeclarationBegin(AstNode *root);

	void ProcessTypeDeclarationEnd(AstNode *root);

	void ProcessNonEmptyRelopExprListBegin(AstNode *root);

	void ProcessNonEmptyRelopExprListEnd(AstNode *root);

	void ProcessDimInfoBegin(AstNode *root);

	void ProcessDimInfoEnd(AstNode *root);

	void ProcessVariableDeclListBegin(AstNode *root);

	void ProcessVariableDeclListEnd(AstNode *root);

	void ProcessNonEmptyAssignExprListBegin(AstNode *root);

	void ProcessNonEmptyAssignExprListEnd(AstNode *root);

	void ProcessStatementListBegin(AstNode *root);

	void ProcessStatementListEnd(AstNode *root);

	void ProcessNulNode(AstNode *root);

	void ProcessConstValueNode(AstNode *root);

	void ProcessIdentifierNode(AstNode *root, bool is_declare);

	void ProcessNodeEnd(AstNode *root);

 private:
	SymbolTable symbol_table;
	ScalarArrayType current_function_return_type;

	std::vector<ScalarArrayType> ProcessNonEmptyRelopExprListNode(AstNode *root);

	bool CheckTruthable(const ScalarArrayType &type);

	void CheckAssignable(AstNode *now, const ScalarArrayType &lhs,
	                     const ScalarArrayType &rhs, bool check_return = false);

	std::vector<int> CollectDimInfo(AstNode *root);

	std::vector<std::pair<ScalarArrayType, std::string>>
	CollectParamDeclarationInfo(AstNode *root);

	void ConstFold(AstNode *root);
};

#endif  // SEMANTIC_ANALYSIS_VIS_HH_
