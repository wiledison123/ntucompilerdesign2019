float f() { return 1; }

int ff() { return 2.0; }

float fff() { return 1; }

int MAIN() {
	write(f());
	write("\n");
	write(ff());
	write("\n");
	write(f() + ff());
	write("\n");
	write(f() - ff());
	write("\n");
	write(f() * ff());
	write("\n");
	write(f() / ff());
	write("\n");
	write(f() > ff());
	write("\n");
	write(f() < ff());
	write("\n");
	write(f() == ff());
	write("\n");
	write(f() != ff());
	write("\n");
	write(f() >= ff());
	write("\n");
	write(f() <= ff());
	write("\n");

	write(ff() + fff());
	write("\n");
	write(ff() - fff());
	write("\n");
	write(ff() * fff());
	write("\n");
	write(ff() / fff());
	write("\n");
	write(ff() > fff());
	write("\n");
	write(ff() < fff());
	write("\n");
	write(ff() == fff());
	write("\n");
	write(ff() != fff());
	write("\n");
	write(ff() >= fff());
	write("\n");
	write(ff() <= fff());
	write("\n");
}
