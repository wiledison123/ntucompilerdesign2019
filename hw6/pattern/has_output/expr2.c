float kAA = 1, kBB = 2.0;
int glob1 = 1, glob2 = 2.0;
float ret_const() {
	return (kAA - kBB) / kAA * kAA + 0.0;
}
void fun() {
	write("FUN\n");
}
int MAIN() {
	{
		int i, j, k;
		float m, n, l;
		k = (1 + 2) * 3 - 4 / 5 + -1;
		k = -k;
		k = k * -1;
		write(k);
		write("\n");

		i = 1;
		j = 2;
		k = 3;
		k = i * j - k * 4 - 5;
		write(k);
		write("\n");

		l = (1.0 + 2.0) * 3.0 - 4.0 / 5.0 + -1.0;
		write(l);
		write("\n");

		m = 1.0;
		n = 2.0;
		l = 3.0;
		l = m * n - l * 4.0 - 5.0;
		write(l);
		write("\n");

		k = 1 > 2;
		write(k);
		write("\n");

		k = 2 >= 2;
		write(k);
		write("\n");

		k = 1 < 2;
		write(k);
		write("\n");

		k = 2 <= 2;
		write(k);
		write("\n");

		k = 1 != 2;
		write(k);
		write("\n");

		k = 1 == 2;
		write(k);
		write("\n");

		k = 1.0 > 2.0;
		write(k);
		write("\n");

		k = 1.0 < 2.0;
		write(k);
		write("\n");

		k = 1.0 != 2.0;
		write(k);
		write("\n");

		k = 5;
		write(!k);
		write("\n");
		write(!(!k));
		write("\n");

		write(-1);
		write("\n");
		write(!5);
		write("\n");
		write(1 || 1);
		write(1 || 0);
		write(0 || 0);
		write(1 && 1);
		write(1 && 0);
		write(0 && 0);
		write("\n");

		{
			float f = 1.0;
			if (f) {
				write("1.0\n");
			}
			f = 0.5;
			if (f) {
				write("0.5\n");
			}
			f = 0.0;
			if (f) {
				write("0.0\n");
			}
		}

		{
			int a = 1, b = 2;
			write(a < b);
			write("\n");
			write(a > b);
			write("\n");
			write(a == b);
			write("\n");
			write(a >= b);
			write("\n");
			write(a <= b);
			write("\n");
		}
		{
			float a = kAA / kBB, b = kAA + kBB;
			write(a < b);
			write("\n");
			write(a > b);
			write("\n");
			write(a == b);
			write("\n");
			write(a >= b);
			write("\n");
			write(a <= b);
			write("\n");
		}
		{
			write(1 < 2);
			write("\n");
			write(1 > 2);
			write("\n");
			write(1 == 2);
			write("\n");
			write(1 >= 2);
			write("\n");
			write(1 <= 2);
			write("\n");
		}
		{
			write(1.0 < 2.0);
			write("\n");
			write(1.0 > 2.0);
			write("\n");
			write(1.0 == 2.0);
			write("\n");
			write(1.0 >= 2.0);
			write("\n");
			write(1.0 <= 2.0);
			write("\n");
		}
	}

	{
		int ff = 0.0;
		write(+ff);
		write(-ff);
		write(!ff);
		write("\n");
	}
	{
		float ff = 0.0;
		write(+ff);
		write(-ff);
		write(!ff);
		write("\n");
	}
	{
		write(+0.0);
		write(-0.0);
		write(!0.0);
		write("\n");
	}
	{
		write(+0);
		write(-0);
		write(!0);
		write("\n");
	}

	write(ret_const());
	write("\n");
	fun();

	return 0;
}
