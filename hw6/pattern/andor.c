int pf, pg;
int f() {
	pf = 1;
	return 1;
}

float g() {
	pg = 1;
	return 3.2;
}

int ret0() {
	write("ret0 called\n");
	return 0;
}

int ret1() {
	write("ret1 called\n");
	return 1;
}

int MAIN() {
	int zero = 0, one = 1;

	if (f() < g()) {
		write("lt\n");
	} else {
		write("ge (wa)\n");
	}

	if (zero && one) {
		write("yay\n");
	} else {
		write("no (wa)\n");
	}

	if (one || zero) {
		write("yay\n");
	} else {
		write("no (wa)\n");
	}

	if (ret0() && ret1()) {
		write("ret1 should not be called\n");
	}

	if (ret1() || ret0()) {
		write("ret0 should not be called\n");
	}
}
