int a[10], b[10][10];

int MAIN() {
	int i, j, k;

	for (i = 0; i < 10; i = i + 1) {
		a[i] = i;
		write("i = ");
		write(i);
		write("\n");
	}

	for (i = 0; i < 10; i = i + 1) {
		for (j = 0; j < 10; j = j + 1) {
			b[i][j] = i * 10 + j;
			write("b[");
			write(i);
			write("][");
			write(j);
			write("] = ");
			write(b[i][j]);
			write("\n");
		}
	}

	for (i = 0; i < 10; i = i + 1) {
		for (j = 0; j < 10; j = j + 1) {
			write("b[");
			write(i);
			write("][a[");
			write(j);
			write("]] = ");
			write(b
					[
					i
					]
					[
					a
						[
						j
						]
					]
					);
			write("\n");
		}
	}
}
