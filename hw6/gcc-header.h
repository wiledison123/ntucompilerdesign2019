#include <stdio.h>
void write(char *s) { printf("%s", s); }
void write(float x) { printf("%f", x); }
void write(int x) { printf("%d", x); }
float _fread() {
	float x;
	scanf("%f", &x);
	return x;
}
int read() {
	int x;
	scanf("%d", &x);
	return x;
}
