#ifndef HEADER_HH_
#define HEADER_HH_

#include <array>
#include <iostream>
#include <optional>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

/* ===== Overloaded Lambda Helper ===== */

template <class... Ts>
struct overload : Ts... {
	using Ts::operator()...;
};
template <class... Ts>
overload(Ts...)->overload<Ts...>;

/* ===== Constants ===== */

#define MAX_ARRAY_DIMENSION 7

enum ArrayDim { ARRAY_DIM_EMPTY = -1 };

/* ===== Simplistic Error Handling ===== */

#ifdef DEBUG
#define debugmsg(_)                                                           \
	std::cerr << "Error: " << __FILE__ << " (" << __func__ << "): " << __LINE__ \
	          << ": " << _
#else
#define debugmsg(_) (3.1415)
#endif

extern bool has_error;

/* ===== Variable Types ===== */

enum DataType {
	NULL_DATATYPE = -1,
	INT_TYPE,
	FLOAT_TYPE,
	VOID_TYPE,
	INT_PTR_TYPE,       // for parameter passing
	FLOAT_PTR_TYPE,     // for parameter passing
	CONST_STRING_TYPE,  // for "const string"
	NONE_TYPE,          // for nodes like PROGRAM_NODE which has no type
	ERROR_TYPE
};

struct ScalarArrayType;
struct FunctionType;
struct TypedefType;
using SymbolType = std::variant<ScalarArrayType, FunctionType, TypedefType>;

// TODO-CLEAN: Something with better type-safety would probably be better.
struct ScalarArrayType {
	DataType datatype;
	std::vector<int> dims;

	bool operator==(const ScalarArrayType &rhs) const;

	ScalarArrayType() = default;

	ScalarArrayType(DataType xType, std::vector<int> xDims)
	    : datatype(xType), dims(xDims) {}

	ScalarArrayType(ScalarArrayType type, std::vector<int> xDims)
	    : datatype(type.datatype) {
		dims = type.dims;
		dims.insert(dims.end(), xDims.begin(), xDims.end());
	}

	ScalarArrayType &operator=(DataType rhs) {
		datatype = rhs;
		dims.clear();
		return *this;
	}
};

struct FunctionType {
	DataType return_type;
	std::vector<ScalarArrayType> args;

	bool operator==(const FunctionType &rhs) const;
};

struct TypedefType {
	// TODO-CLEAN:
	// https://stackoverflow.com/questions/15005809/idiom-for-strict-typedef-in-c
	ScalarArrayType realtype;

	bool operator==(const TypedefType &rhs) const;
};

/* ===== AstNode-related Enums ===== */

enum IdKind {
	NORMAL_ID,    // function Name, uninitialized scalar variable
	ARRAY_ID,     // ID_NODE->child = dim
	WITH_INIT_ID  // ID_NODE->child = initial value
};

enum Operator {
	BINARY_OP_ADD,
	BINARY_OP_SUB,
	BINARY_OP_MUL,
	BINARY_OP_DIV,
	BINARY_OP_EQ,
	BINARY_OP_GE,
	BINARY_OP_LE,
	BINARY_OP_NE,
	BINARY_OP_GT,
	BINARY_OP_LT,
	BINARY_OP_AND,
	BINARY_OP_OR,
	UNARY_BASE,
	UNARY_OP_POSITIVE = UNARY_BASE,
	UNARY_OP_NEGATIVE,
	UNARY_OP_LOGICAL_NEGATION
};

enum StmtKind {
	WHILE_STMT,
	FOR_STMT,
	ASSIGN_STMT,  // TODO-TA: for simpler implementation, assign_expr also uses
	              // this
	IF_STMT,
	FUNCTION_CALL_STMT,
	RETURN_STMT
};

enum ExprKind { BINARY_OPERATION, UNARY_OPERATION };

enum DeclKind {
	VARIABLE_DECL,
	TYPE_DECL,
	FUNCTION_DECL,
	FUNCTION_PARAMETER_DECL
};

enum AstType {
	NULL_ASTTYPE = -1,
	PROGRAM_NODE,
	DECLARATION_NODE,
	IDENTIFIER_NODE,
	PARAM_LIST_NODE,
	NUL_NODE,
	BLOCK_NODE,
	VARIABLE_DECL_LIST_NODE,
	STMT_LIST_NODE,
	STMT_NODE,
	EXPR_NODE,
	CONST_VALUE_NODE,  // ex: 1.0, 2, "constant string"
	NONEMPTY_ASSIGN_EXPR_LIST_NODE,
	NONEMPTY_RELOP_EXPR_LIST_NODE
};

/* ===== Human-readable Enum Names ===== */

const std::array<std::string, 8> DataType2str{
    "INT_TYPE",       "FLOAT_TYPE",        "VOID_TYPE", "INT_PTR_TYPE",
    "FLOAT_PTR_TYPE", "CONST_STRING_TYPE", "NONE_TYPE", "ERROR_TYPE"};
const std::array<std::string, 3> IdKind2str{"NORMAL_ID", "ARRAY_ID",
                                            "WITH_INIT_ID"};
const std::array<std::string, 16> Operator2str{
    "BINARY_OP_ADD",     "BINARY_OP_SUB",     "BINARY_OP_MUL",
    "BINARY_OP_DIV",     "BINARY_OP_EQ",      "BINARY_OP_GE",
    "BINARY_OP_LE",      "BINARY_OP_NE",      "BINARY_OP_GT",
    "BINARY_OP_LT",      "BINARY_OP_AND",     "BINARY_OP_OR",
    "UNARY_OP_POSITIVE", "UNARY_OP_NEGATIVE", "UNARY_OP_LOGICAL_NEGATION"};
const std::array<std::string, 6> StmtKind2str{
    "WHILE_STMT", "FOR_STMT",           "ASSIGN_STMT",
    "IF_STMT",    "FUNCTION_CALL_STMT", "RETURN_STMT"};
const std::array<std::string, 2> ExprKind2str{"BINARY_OPERATION",
                                              "UNARY_OPERATION"};
const std::array<std::string, 4> DeclKind2str{
    "VARIABLE_DECL", "TYPE_DECL", "FUNCTION_DECL", "FUNCTION_PARAMETER_DECL"};
const std::array<std::string, 13> AstType2str{"PROGRAM_NODE",
                                              "DECLARATION_NODE",
                                              "IDENTIFIER_NODE",
                                              "PARAM_LIST_NODE",
                                              "NUL_NODE",
                                              "BLOCK_NODE",
                                              "VARIABLE_DECL_LIST_NODE",
                                              "STMT_LIST_NODE",
                                              "STMT_NODE",
                                              "EXPR_NODE",
                                              "CONST_VALUE_NODE",
                                              "NONEMPTY_ASSIGN_EXPR_LIST_NODE",
                                              "NONEMPTY_RELOP_EXPR_LIST_NODE"};

/* ===== AstNode Semantic Value ===== */

struct StmtSemanticVal {
	StmtKind kind;
};

struct ExprSemanticVal {
	ExprKind kind;
	int isConstEval;
	std::variant<int, float> constEvalValue;
	Operator op;

	ExprSemanticVal() : kind(), isConstEval(), constEvalValue(), op() {}
	ExprSemanticVal(ExprKind xKind, bool xIsConstEval, Operator xOp)
	    : kind(xKind), isConstEval(xIsConstEval), constEvalValue(), op(xOp) {}
};

struct DeclSemanticVal {
	DeclKind kind;
};

struct IdSemanticVal {
	std::string identifierName;
	// SymbolTableEntry *symbolTableEntry;
	IdKind kind;
};

struct TypeSpecSemanticValue {
	std::string typeName;
};

using Constant = std::variant<double, int, std::string>;

/* ===== AstNode ===== */

struct AstNode {
	// Semantic Analysis
	AstNode *child, *parent, *rightSibling, *leftmostSibling;
	AstType nodeType;
	ScalarArrayType dataType;
	int line_number;
	std::variant<IdSemanticVal, StmtSemanticVal, DeclSemanticVal, ExprSemanticVal,
	             Constant>
	    semantic_value;
	// Pregenerator
	bool is_global, is_leaf_func, is_param;
	int stack_offset;
	int dim_tmp_offset;
	// Generator
	std::stringstream code;
	FunctionType function_param_type;

	AstNode()
	    : child(nullptr),
	      parent(nullptr),
	      rightSibling(nullptr),
	      leftmostSibling(nullptr),
	      nodeType(NULL_ASTTYPE),
	      dataType{NULL_DATATYPE, {}},
	      line_number(-1),
	      semantic_value(),
	      is_global(false),
	      is_leaf_func(true),
	      is_param(false),
	      stack_offset(0),
	      dim_tmp_offset(0),
	      function_param_type() {}
};

#endif
