#include <iostream>
#include <utility>

#include "header.hh"

void PrintAst(std::ostream &o, int dep, AstNode *now) {
	if (now == nullptr) return;
	o << now << ": ";
	for (int i = 0; i < dep; ++i) o << " ";
	o << AstType2str[now->nodeType] << ' ';
	if (now->nodeType == DECLARATION_NODE)
		o << DeclKind2str[std::get<DeclSemanticVal>(now->semantic_value).kind];
	else if (now->nodeType == IDENTIFIER_NODE) {
		o << std::get<IdSemanticVal>(now->semantic_value).identifierName << " "
		  << IdKind2str[std::get<IdSemanticVal>(now->semantic_value).kind];
	} else if (now->nodeType == STMT_NODE)
		o << StmtKind2str[std::get<StmtSemanticVal>(now->semantic_value).kind];
	else if (now->nodeType == EXPR_NODE)
		o << Operator2str[std::get<ExprSemanticVal>(now->semantic_value).op];
	else if (now->nodeType == CONST_VALUE_NODE)
		std::visit([&](auto x) { o << x; },
		           std::get<Constant>(now->semantic_value));
	o << "\t offset " << now->stack_offset;
	o << "\t is globacl " << now->is_global;
	o << "\t dim_offset " << now->dim_tmp_offset;
	o << "\t" << DataType2str[now->dataType.datatype] << " with dim: ";
	for (int i : now->dataType.dims) o << i << ' '; 
	o << std::endl;
	PrintAst(o, dep + 1, now->child);
	PrintAst(o, dep, now->rightSibling);
}
