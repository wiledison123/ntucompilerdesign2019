#include <array>
#include <cstdlib>

#include "header.hh"

extern int line_number;

AstNode *Allocate(AstType type) {
	AstNode *temp;
	temp = new AstNode();
	temp->nodeType = type;
	temp->dataType = NONE_TYPE;
	// Notice that leftmostSibling is not initialized as nullptr
	temp->leftmostSibling = temp;
	temp->line_number = line_number;
	return temp;
}
