#include "generator.hh"

PreGenerator::PreGenerator()
	: symbol_table{},
	  stack_offset_record{},
	  current_function_astnode{},
	  current_stack_offset{-16},
	  maximum_offset{-16},
	  current_function_params{} {}

void PreGenerator::ProcessBinaryExprNode(AstNode *root) {
	root->stack_offset = GetStackSpace(1);
	fprintf(stderr, "GetStackSpace %d for binary expr in line %d\n",
			GetStackSpace(0), root->line_number);
}

void PreGenerator::ProcessUnaryExprNode(AstNode *root) {
	root->stack_offset = GetStackSpace(1);
	fprintf(stderr, "GetStackSpace %d for binary expr in line %d\n",
			GetStackSpace(0), root->line_number);
}

void PreGenerator::ProcessReturnStatementNode(AstNode *root) { (void)root; }

void PreGenerator::ProcessFunctionCallStatementNode(AstNode *root) {
	current_function_astnode->is_leaf_func = false;
	root->stack_offset = GetStackSpace(1);  // store returned value
	fprintf(stderr, "GetStackSpace %d for func call in line %d\n",
			GetStackSpace(0), root->line_number);
	AstNode *id_node = root->child;
	if (std::string name =
			std::get<IdSemanticVal>(id_node->semantic_value).identifierName;
		name != "write")
		id_node->function_param_type =
			std::get<FunctionType>(symbol_table.GetVariableType(id_node, name));
}

void PreGenerator::ProcessIfStatementNode(AstNode *root) { (void)root; }
void PreGenerator::ProcessAssignStatementNode(AstNode *root) { (void)root; }
void PreGenerator::ProcessForStatementNode(AstNode *root) { (void)root; }
void PreGenerator::ProcessWhileStatementNode(AstNode *root) { (void)root; }

void PreGenerator::ProcessBlockBegin(AstNode *root, bool open_scope) {
	(void)root;
	if (open_scope) {
		SnapshotStackOffset();
		symbol_table.OpenScope();
	}
}

void PreGenerator::ProcessBlockEnd(AstNode *root, bool open_scope) {
	(void)root;
	if (open_scope) {
		RestoreStackOffset();
		symbol_table.CloseScope();
	}
}

void PreGenerator::ProcessFunctionDeclBegin(AstNode *root) {
	assert(current_stack_offset == -16);

	SnapshotStackOffset();
	auto _root = root;

	root = root->child;

	auto real_type = root->dataType;
	root = root->rightSibling;
	std::string name =
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root = root->rightSibling;
	std::vector<ScalarArrayType> param;
	auto param_node = root->child;
	while (param_node) {
		param.push_back(param_node->dataType);
		param_node = param_node->rightSibling;
	}
	symbol_table.InsertVariable(root, FunctionType{real_type.datatype, param},
								name);
	symbol_table.OpenScope();

	root = _root;

	current_function_astnode = root;
	int i_param_cnt = 0, f_param_cnt = 0, stack_ofs = 0;
	param_node =
		current_function_astnode->child->rightSibling->rightSibling->child;
	// TODO: Should we count ints and floats separately or together?
	while (param_node) {
		bool is_f = param_node->dataType.datatype == FLOAT_TYPE &&
					param_node->dataType.dims.empty();
		if ((is_f ? f_param_cnt : i_param_cnt) < 8) {
			// TODO-IMPROVE: Only allocate 8 when necessary.
			param_node->stack_offset = GetStackSpace(2);
		} else {
			param_node->stack_offset = stack_ofs;
			stack_ofs += param_node->dataType.dims.size() ? 8 : 4;
		}
		name = std::get<IdSemanticVal>(
				   param_node->child->rightSibling->semantic_value)
				   .identifierName;
		symbol_table.InsertVariable(param_node, param_node->dataType, name,
									param_node->stack_offset);
		current_function_params.insert(name);
		if (is_f)
			++f_param_cnt;
		else
			++i_param_cnt;
		param_node = param_node->rightSibling;
	}
}

void PreGenerator::ProcessFunctionDeclEnd(AstNode *root) {
	(void)root;
	RestoreStackOffset();
	std::cerr << "fsef" << maximum_offset << std::endl;
	current_function_astnode->stack_offset =
		maximum_offset ^ (maximum_offset & 15);
	current_function_astnode = nullptr;
	current_function_params.clear();
	maximum_offset = -16;
	symbol_table.CloseScope();
}

void PreGenerator::ProcessDeclBegin(AstNode *root) {
	std::string name =
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	int dim_boost = 1;
	for (int i : root->dataType.dims) dim_boost *= i;

	root->is_global = current_function_astnode == nullptr;
	if (int knd = std::get<IdSemanticVal>(root->semantic_value).kind;
		knd == WITH_INIT_ID || knd == NORMAL_ID) {
		int ofs = root->stack_offset =
			(current_function_astnode ? GetStackSpace(1 * dim_boost) : 0);
		fprintf(stderr, "GetStackSpace %d in line %d\n", ofs, root->line_number);
		symbol_table.InsertVariable(root, root->dataType, name, ofs);
	} else {
		int ofs = root->stack_offset =
			(current_function_astnode ? GetStackSpace(dim_boost) : 0);
		fprintf(stderr, "GetStackSpace %d in line %d\n", ofs, root->line_number);
		symbol_table.InsertVariable(root, root->dataType, name, ofs);
	}
}

void PreGenerator::ProcessNulNode(AstNode *root) { root->dataType = VOID_TYPE; }

void PreGenerator::ProcessConstValueNode(AstNode *root) {
	if (current_function_astnode == nullptr) return;
	root->stack_offset = GetStackSpace(1);
	fprintf(stderr, "GetStackSpace %d for const in line %d\n", GetStackSpace(0),
			root->line_number);
}

void PreGenerator::ProcessIdentifierNode(AstNode *root, bool is_declare) {
	std::string name =
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	root->stack_offset = symbol_table.GetOffset(root, name);
	if (!is_declare) {
		int scope_level = symbol_table.GetScopeLevel(root, name);
		root->is_global = scope_level == 0;
		root->dataType =
			std::get<ScalarArrayType>(symbol_table.GetVariableType(root, name));
		assert(root->dataType.datatype != NONE_TYPE);
		if (std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID)
			root->dim_tmp_offset = GetStackSpace(1);
		root->is_param = scope_level == 1 && current_function_params.count(name);
	}
}

void PreGenerator::ProcessDeclEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessNonEmptyRelopExprListBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessNonEmptyRelopExprListEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessDimInfoBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessDimInfoEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessVariableDeclListBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessVariableDeclListEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessNonEmptyAssignExprListBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessNonEmptyAssignExprListEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessStatementListBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessStatementListEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessNodeEnd(AstNode *root) { (void)root; }
void PreGenerator::ProcessTypeDeclarationBegin(AstNode *root) { (void)root; }
void PreGenerator::ProcessTypeDeclarationEnd(AstNode *root) { (void)root; }

void PreGenerator::SnapshotStackOffset() { 
	stack_offset_record.push(current_stack_offset); 
}

void PreGenerator::RestoreStackOffset() {
	assert(stack_offset_record.size() &&
		   "Tried to RestoreStackOffset when stack_offset_record is empty");
	maximum_offset = std::min(maximum_offset, current_stack_offset);
	current_stack_offset = stack_offset_record.top();
	stack_offset_record.pop();
}

int PreGenerator::GetStackSpace(int x) {
	current_stack_offset -= x * 4;
	return current_stack_offset;
}

void Generator::ProcessBinaryExprNode(AstNode *root) {
	auto &o = root->code;
	if (root->dataType.datatype == FLOAT_TYPE) {
		o << root->child->code.str();
		o << root->child->rightSibling->code.str();
		root->child->code.str("");
		root->child->rightSibling->code.str("");

		ResolveAddress(root->child, o, "t2");
		if (root->child->dataType.datatype == INT_TYPE) {
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tfcvt.s.w\tft0,t0" << std::endl;
		} else {
			o << "\tflw\tft0,0(t2)" << std::endl;
		}
		ResolveAddress(root->child->rightSibling, o, "t2");
		if (root->child->rightSibling->dataType.datatype == INT_TYPE) {
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tfcvt.s.w\tft1,t0" << std::endl;
		} else {
			o << "\tflw\tft1,0(t2)" << std::endl;
		}

		switch (std::get<ExprSemanticVal>(root->semantic_value).op) {
			case BINARY_OP_ADD:
				o << "\tfadd.s\tft0,ft0,ft1" << std::endl;
				break;
			case BINARY_OP_SUB:
				o << "\tfsub.s\tft0,ft0,ft1" << std::endl;
				break;
			case BINARY_OP_MUL:
				o << "\tfmul.s\tft0,ft0,ft1" << std::endl;
				break;
			case BINARY_OP_DIV:
				o << "\tfdiv.s\tft0,ft0,ft1" << std::endl;
				break;
			default:
				assert(0);  // Should not happen
		}

		ResolveAddress(root, o, "t2");
		o << "\tfsw\tft0,0(t2)" << std::endl;
	} else if (root->child->dataType.datatype == INT_TYPE &&
			   root->child->rightSibling->dataType.datatype == INT_TYPE) {
		if (int op = std::get<ExprSemanticVal>(root->semantic_value).op;
			op == BINARY_OP_AND) {
			static int and_count = 0;

			o << root->child->code.str();
			root->child->code.str("");
			ResolveAddress(root->child, o, "t2");
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tbeqz\tt0," << kAndSkipPrefix << and_count << std::endl;

			o << root->child->rightSibling->code.str();
			root->child->rightSibling->code.str("");
			ResolveAddress(root->child->rightSibling, o, "t2");
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tbeqz\tt0," << kAndSkipPrefix << and_count << std::endl;

			ResolveAddress(root, o, "t2");
			o << "\tli\tt0,1" << std::endl;
			o << "\tsw\tt0,0(t2)" << std::endl;
			o << "\tj\t" << kAndEndPrefix << and_count << std::endl;

			o << kAndSkipPrefix << and_count << ":" << std::endl;
			ResolveAddress(root, o, "t2");
			o << "\tli\tt0,0" << std::endl;
			o << "\tsw\tt0,0(t2)" << std::endl;
			o << kAndEndPrefix << and_count << ":" << std::endl;

			++and_count;
		} else if (op == BINARY_OP_OR) {
			static int or_count = 0;

			o << root->child->code.str();
			root->child->code.str("");
			ResolveAddress(root->child, o, "t2");
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tbnez\tt0," << kOrSkipPrefix << or_count << std::endl;

			o << root->child->rightSibling->code.str();
			root->child->rightSibling->code.str("");
			ResolveAddress(root->child->rightSibling, o, "t2");
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tbnez\tt0," << kOrSkipPrefix << or_count << std::endl;

			ResolveAddress(root, o, "t2");
			o << "\tli\tt0,0" << std::endl;
			o << "\tsw\tt0,0(t2)" << std::endl;
			o << "\tj\t" << kOrEndPrefix << or_count << std::endl;

			o << kOrSkipPrefix << or_count << ":" << std::endl;
			ResolveAddress(root, o, "t2");
			o << "\tli\tt0,1" << std::endl;
			o << "\tsw\tt0,0(t2)" << std::endl;
			o << kOrEndPrefix << or_count << ":" << std::endl;

			++or_count;
		} else {
			o << root->child->code.str();
			o << root->child->rightSibling->code.str();
			root->child->code.str("");
			root->child->rightSibling->code.str("");

			ResolveAddress(root->child, o, "t2");
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			ResolveAddress(root->child->rightSibling, o, "t2");
			o << "\tlw\tt1,0(t2)" << std::endl;
			o << "\tsext.w\tt1,t1" << std::endl;

			switch (op) {
				case BINARY_OP_ADD:
					o << "\taddw\tt0,t0,t1" << std::endl;
					break;
				case BINARY_OP_SUB:
					o << "\tsubw\tt0,t0,t1" << std::endl;
					break;
				case BINARY_OP_MUL:
					o << "\tmulw\tt0,t0,t1" << std::endl;
					break;
				case BINARY_OP_DIV:
					o << "\tdivw\tt0,t0,t1" << std::endl;
					break;
				case BINARY_OP_EQ:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tsub\tt0,t0,t1" << std::endl;
					o << "\tseqz\tt0,t0" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				case BINARY_OP_GE:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tslt\tt0,t0,t1" << std::endl;
					o << "\txori\tt0,t0,1" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				case BINARY_OP_LE:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tsgt\tt0,t0,t1" << std::endl;
					o << "\txori\tt0,t0,1" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				case BINARY_OP_NE:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tsub\tt0,t0,t1" << std::endl;
					o << "\tsnez\tt0,t0" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				case BINARY_OP_GT:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tsgt\tt0,t0,t1" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				case BINARY_OP_LT:
					o << "\tsext.w\tt0,t0" << std::endl;
					o << "\tsext.w\tt1,t1" << std::endl;
					o << "\tslt\tt0,t0,t1" << std::endl;
					o << "\tandi\tt0,t0,0xff" << std::endl;
					break;
				default:
					assert(0);  // Should not happen
			}

			ResolveAddress(root, o, "t2");
			o << "\tsw\tt0,0(t2)" << std::endl;
		}
	} else {
		o << root->child->code.str();
		o << root->child->rightSibling->code.str();
		root->child->code.str("");
		root->child->rightSibling->code.str("");

		ResolveAddress(root->child, o, "t2");
		if (root->child->dataType.datatype == INT_TYPE) {
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tfcvt.s.w\tft0,t0" << std::endl;
		} else {
			o << "\tflw\tft0,0(t2)" << std::endl;
		}
		ResolveAddress(root->child->rightSibling, o, "t2");
		if (root->child->rightSibling->dataType.datatype == INT_TYPE) {
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0,t0" << std::endl;
			o << "\tfcvt.s.w\tft1,t0" << std::endl;
		} else {
			o << "\tflw\tft1,0(t2)" << std::endl;
		}

		switch (std::get<ExprSemanticVal>(root->semantic_value).op) {
			case BINARY_OP_EQ:
				o << "\tfeq.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			case BINARY_OP_GE:
				o << "\tfge.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			case BINARY_OP_LE:
				o << "\tfle.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			case BINARY_OP_NE:
				o << "\tfeq.s\tt0,ft0,ft1" << std::endl;
				o << "\tseqz\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			case BINARY_OP_GT:
				o << "\tfgt.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			case BINARY_OP_LT:
				o << "\tflt.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			default:
				assert(0);  // Should not happen
		}

		ResolveAddress(root, o, "t2");
		o << "\tsw\tt0,0(t2)" << std::endl;
	}
}

void Generator::ProcessUnaryExprNode(AstNode *root) {
	auto &o = root->code;
	o << root->child->code.str();
	root->child->code.str("");
	if (root->dataType.datatype == FLOAT_TYPE) {
		ResolveAddress(root->child, o, "t2");
		o << "\tflw\tft0,0(t2)" << std::endl;

		switch (std::get<ExprSemanticVal>(root->semantic_value).op) {
			case UNARY_OP_POSITIVE:
				break;
			case UNARY_OP_NEGATIVE:
				o << "\tfneg.s ft0,ft0" << std::endl;
				break;
			default:
				assert(0);  // Should not happen
		}

		ResolveAddress(root, o, "t2");
		o << "\tfsw\tft0,0(t2)" << std::endl;
	} else if (root->child->dataType.datatype == INT_TYPE) {
		ResolveAddress(root->child, o, "t2");
		o << "\tlw\tt0,0(t2)" << std::endl;
		o << "\tsext.w\tt0,t0" << std::endl;

		switch (std::get<ExprSemanticVal>(root->semantic_value).op) {
			case UNARY_OP_POSITIVE:
				break;
			case UNARY_OP_NEGATIVE:
				o << "\tsubw\tt0,zero,t0" << std::endl;
				break;
			case UNARY_OP_LOGICAL_NEGATION:
				o << "\tsext.w\tt0,t0" << std::endl;
				o << "\tseqz\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			default:
				assert(0);  // Should not happen
		}

		ResolveAddress(root, o, "t2");
		o << "\tsw\tt0,0(t2)" << std::endl;
	} else {
		ResolveAddress(root->child, o, "t2");
		o << "\tflw\tft0,0(t2)" << std::endl;

		switch (std::get<ExprSemanticVal>(root->semantic_value).op) {
			case UNARY_OP_LOGICAL_NEGATION:
				o << "\tfmv.s.x\tft1,zero" << std::endl;
				o << "\tfeq.s\tt0,ft0,ft1" << std::endl;
				o << "\tsnez\tt0,t0" << std::endl;
				o << "\tandi\tt0,t0,0xff" << std::endl;
				break;
			default:
				assert(0);  // Should not happen
		}

		ResolveAddress(root, o, "t2");
		o << "\tsw\tt0,0(t2)" << std::endl;
	}
}

void Generator::ProcessReturnStatementNode(AstNode *root) {
	ConsumeChildCode(root);
	auto &o = root->code;
	std::string name =
		std::get<IdSemanticVal>(
			current_function_node->child->rightSibling->semantic_value)
			.identifierName;
	if (root->child->dataType.datatype != VOID_TYPE) {
		ResolveAddress(root->child, o, "t2");
		if (root->child->dataType.datatype == FLOAT_TYPE) {
			o << "\tflw\tft0,0(t2)" << std::endl;
			if (current_function_node->child->dataType.datatype == FLOAT_TYPE) {
				o << "\tfmv.s\tfa0, ft0" << std::endl;
			} else {
				o << "\tfcvt.w.s\ta0, ft0, rtz" << std::endl;
			}
		} else {
			o << "\tlw\tt0,0(t2)" << std::endl;
			o << "\tsext.w\tt0, t0" << std::endl;
			if (current_function_node->child->dataType.datatype == FLOAT_TYPE) {
				o << "\tfcvt.s.w\tfa0, t0" << std::endl;
			} else {
				o << "\tmv\ta0,t0" << std::endl;
			}
		}
	}
	o << "\tj\t" << kReturnLabelPrefix << name << std::endl;
}

void Generator::ProcessFunctionCallStatementNode(AstNode *root) {
	ConsumeChildCode(root);
	auto &o = root->code;
	std::string name =
		std::get<IdSemanticVal>(root->child->semantic_value).identifierName;
	if (name == "read") {
		o << "\tcall\t_read_int" << std::endl;
		ResolveAddress(root, o, "t2");
		o << "\tsw\ta0, 0(t2)" << std::endl;
	} else if (name == "fread") {
		o << "\tcall\t_read_float" << std::endl;
		ResolveAddress(root, o, "t2");
		o << "\tfsw\tfa0, 0(t2)" << std::endl;
	} else if (name == "write") {
		auto arg = root->child->rightSibling->child;
		if (arg->dataType.datatype == FLOAT_TYPE) {
			ResolveAddress(arg, o, "t2");
			o << "\tflw\tfa0,0(t2)" << std::endl;
			o << "\tjal\t_write_float" << std::endl;
		} else if (arg->dataType.datatype == INT_TYPE) {
			ResolveAddress(arg, o, "t2");
			o << "\tlw\ta0,0(t2)" << std::endl;
			o << "\tsext.w\ta0,a0" << std::endl;
			o << "\tjal\t_write_int" << std::endl;
		} else {
			o << "\tlui\ta5, %hi(" << kGlobalStrPrefix << arg->stack_offset << ")"
			  << std::endl;
			o << "\taddi\ta0, a5, %lo(" << kGlobalStrPrefix << arg->stack_offset
			  << ")" << std::endl;
			o << "\tcall\t_write_str" << std::endl;
		}
	} else {
		std::vector<AstNode *> params;
		AstNode *now = root->child->rightSibling->child;
		while (now) {
			params.push_back(now);
			now = now->rightSibling;
		}
		std::vector<int> has_register(params.size(), -1);
		std::vector<int> stack_position(params.size(), 0);
		
		int c_i = 0, c_f = 0;
		int stack_change = 0;
		for (auto i = 0ul; i < params.size(); ++i) {
			if (params[i]->dataType.dims.size() || 
					root->child->function_param_type.args[i].datatype == INT_TYPE) {
				if (c_i < 8) {
					has_register[i] = c_i;
					++c_i;
				} else if (params[i]->dataType.dims.size()) 
					stack_change += 8;
				else stack_change += 4;
			} else {
				if (c_f < 8) {
					has_register[i] = c_f;
					++c_f;
				} else stack_change += 4;
			}
		}

		int now_stack_change = stack_change;
		for (int i = static_cast<int>(params.size()) - 1; i >= 0; --i) {
			if (has_register[i] == -1) {
				if (params[i]->dataType.dims.size()) 
					now_stack_change -= 8;
				else now_stack_change -= 4;
				stack_position[i] = now_stack_change;
			}
		}

		if (stack_change) GenerateAddi(o, "sp", "sp", -stack_change);

		for (auto i = 0u; i < params.size(); ++i) {
			ResolveAddress(params[i], o, "t2");
			if (params[i]->dataType.dims.size()) {
				if (has_register[i] != -1) 
					o << "\tmv\ta" << has_register[i] << ",t2" << std::endl;
				else 
					GenerateSd(o, "t2", stack_position[i], "sp");
			} else if (params[i]->dataType.datatype == INT_TYPE) {
				if (has_register[i] != -1) {
					if (root->child->function_param_type.args[i].datatype == INT_TYPE) {
						o << "\tlw\ta" << has_register[i] << ",0(t2)" << std::endl;
					} else {
						o << "\tlw\tt0,0(t2)" << std::endl;
						o << "\tfcvt.s.w\tfa" << has_register[i] << ",t0" << std::endl;
					}
				} else {
					if (root->child->function_param_type.args[i].datatype == INT_TYPE) {
						o << "\tlw\tt2,0(t2)" << std::endl;
						GenerateSw(o, "t2", stack_position[i], "sp");
					} else {
						o << "\tlw\tt2,0(t2)" << std::endl;
						o << "\tfcvt.s.w\tft2,t2" << std::endl;
						GenerateFsw(o, "ft2", stack_position[i], "sp");
					}
				}
			} else {
				if (has_register[i] != -1) {
					if (root->child->function_param_type.args[i].datatype == INT_TYPE) {
						o << "\tflw\tft2,0(t2)" << std::endl;
						o << "\tfcvt.w.s\ta" << has_register[i] << ",ft2, rtz" << std::endl;
					} else {
						o << "\tflw\tfa" << has_register[i] << ",0(t2)" << std::endl;
					}
				} else {
					if (root->child->function_param_type.args[i].datatype == INT_TYPE) {
						o << "\tflw\tft2,0(t2)" << std::endl;
						o << "\tfcvt.w.s\tt2,ft2, rtz" << std::endl;
						GenerateSw(o, "t2", stack_position[i], "sp");
					} else {
						o << "\tflw\tft2,0(t2)" << std::endl;
						GenerateFsw(o, "ft2", stack_position[i], "sp");
					}
				}
			}
		}

		o << "\tcall\t" << kFuncLabelPrefix << name << std::endl;
		if (stack_change) GenerateAddi(o, "sp", "sp", stack_change);

		ResolveAddress(root, o, "t2");
		if (root->dataType.datatype == FLOAT_TYPE) 
			o << "\tfsw\tfa0, 0(t2)" << std::endl;
		else if (root->dataType.datatype == INT_TYPE) 
			o << "\tsw\ta0, 0(t2)" << std::endl;
	}
}

void Generator::ProcessIfStatementNode(AstNode *root) {
	static int if_stmt_cnt = 0;
	auto &o = root->code;
	AstNode *test = root->child, *bloc = test->rightSibling,
			*esle = bloc->rightSibling;
	o << test->code.str();
	test->code.str("");
	ProcessTestNode(test, o, kIfElsePrefix + std::to_string(if_stmt_cnt));
	o << bloc->code.str();
	bloc->code.str("");
	o << "\tj\t" << kIfEndPrefix << if_stmt_cnt << std::endl;
	o << kIfElsePrefix << if_stmt_cnt << ":" << std::endl;
	if (esle) {
		o << esle->code.str();
		esle->code.str("");
	}
	o << kIfEndPrefix << if_stmt_cnt << ":" << std::endl;
	++if_stmt_cnt;
}

void Generator::ProcessAssignStatementNode(AstNode *root) {
	ConsumeChildCode(root);
	AssignImpl(root->child, root->child->rightSibling, root->code);
}

void Generator::ProcessForStatementNode(AstNode *root) {
	static int for_stmt_cnt = 0;
	auto &o = root->code;
	AstNode *init = root->child, *test = init->rightSibling,
			*incr = test->rightSibling, *bloc = incr->rightSibling;
	o << init->code.str();
	init->code.str("");
	o << kForTestPrefix << for_stmt_cnt << ":" << std::endl;
	o << test->code.str();
	test->code.str("");
	ProcessTestNode(test, o, kForEndPrefix + std::to_string(for_stmt_cnt));
	o << bloc->code.str();
	bloc->code.str("");
	o << incr->code.str();
	incr->code.str("");
	o << "\tj\t" << kForTestPrefix << for_stmt_cnt << std::endl;
	o << kForEndPrefix << for_stmt_cnt << ":" << std::endl;
	++for_stmt_cnt;
}

void Generator::ProcessWhileStatementNode(AstNode *root) {
	static int while_stmt_cnt = 0;
	auto &o = root->code;
	AstNode *test = root->child, *bloc = test->rightSibling;
	o << kWhileTestPrefix << while_stmt_cnt << ":" << std::endl;
	o << test->code.str();
	test->code.str("");
	ProcessTestNode(test, o, kWhileEndPrefix + std::to_string(while_stmt_cnt));
	o << bloc->code.str();
	bloc->code.str("");
	o << "\tj\t" << kWhileTestPrefix << while_stmt_cnt << std::endl;
	o << kWhileEndPrefix << while_stmt_cnt << ":" << std::endl;
	++while_stmt_cnt;
}

void Generator::ProcessBlockBegin(AstNode *root, bool open_scope) {
	(void)root;
	(void)open_scope;
}

void Generator::ProcessBlockEnd(AstNode *root, bool open_scope) {
	(void)root;
	(void)open_scope;
}

void Generator::ProcessFunctionDeclBegin(AstNode *root) {
	ConsumeChildCode(root);
	current_function_node = root;
	std::string name =
		std::get<IdSemanticVal>(
			current_function_node->child->rightSibling->semantic_value)
			.identifierName;
	auto &o = root->code;
	o << kFuncLabelPrefix << name << ":" << std::endl;
	GenerateAddi(o, "sp", "sp", root->stack_offset);
	if (!root->is_leaf_func) {
		GenerateSd(o, "ra", -root->stack_offset - 8, "sp");
	}
	GenerateSd(o, "s0", -root->stack_offset - 16, "sp");
	GenerateAddi(o, "s0", "sp", -root->stack_offset);
	int i_param_cnt = 0, f_param_cnt = 0;
	AstNode *param_node =
		current_function_node->child->rightSibling->rightSibling->child;
	while (param_node) {
		bool is_f = param_node->dataType.datatype == FLOAT_TYPE &&
					param_node->dataType.dims.empty();
		if (is_f) {
			if (f_param_cnt < 8) {
				GenerateFsw(o, "fa" + std::to_string(f_param_cnt), param_node->stack_offset, "s0");
			}
			++f_param_cnt;
		} else {
			if (i_param_cnt < 8) {
				GenerateSd(o, "a" + std::to_string(i_param_cnt), param_node->stack_offset, "s0");
			}
			++i_param_cnt;
		}
		param_node = param_node->rightSibling;
	}
}

void Generator::ProcessFunctionDeclEnd(AstNode *root) {
	ConsumeChildCode(root);
	auto &o = root->code;
	std::string name =
		std::get<IdSemanticVal>(
			current_function_node->child->rightSibling->semantic_value)
			.identifierName;
	current_function_node = nullptr;
	o << kReturnLabelPrefix << name << ":" << std::endl;
	if (!root->is_leaf_func) {
		GenerateLd(o, "ra", -root->stack_offset - 8, "sp");
	}
	GenerateLd(o, "s0", -root->stack_offset - 16, "sp");
	GenerateAddi(o, "sp", "sp", -root->stack_offset);
	o << "\tjr\tra" << std::endl;
}

void Generator::ProcessDeclBegin(AstNode *root) { (void)root; }

void Generator::ProcessDeclEnd(AstNode *root) {
	ConsumeChildCode(root);
	auto name = std::get<IdSemanticVal>(root->semantic_value).identifierName;
	if (int knd = std::get<IdSemanticVal>(root->semantic_value).kind;
		knd == WITH_INIT_ID) {
		if (root->is_global) {
			int val = 0;
			if (root->dataType.datatype == FLOAT_TYPE) {
				float f_val = std::visit(
					overload{[](double x) { return float(x); },
							 [](int x) { return float(x); },
							 [](std::string x) -> float { assert(((void)x, 0)); }},
					std::get<Constant>(root->child->semantic_value));
				static_assert(sizeof(f_val) <= sizeof(val));
				memcpy(&val, &f_val, sizeof(f_val));
			} else {
				val = std::visit(
					overload{[](double x) { return int(x); },
							 [](int x) { return int(x); },
							 [](std::string x) -> int { assert(((void)x, 0)); }},
					std::get<Constant>(root->child->semantic_value));
			}
			global_data << ".data\n"
						<< kGlobalVarPrefix << name << ":\t.word\t" << val
						<< "\n.text" << std::endl;
		} else {
			AssignImpl(root, root->child, root->code);
		}
	} else if (root->is_global) {
		std::vector<int> dims = root->dataType.dims;
		int total_size = 4;
		for (int i : dims) total_size *= i;
		global_data << ".data\n"
					<< kGlobalVarPrefix << name << ":\t.zero\t" << total_size
					<< "\n.text" << std::endl;
	}
}

void Generator::ProcessNulNode(AstNode *root) { (void)root; }

void Generator::ProcessConstValueNode(AstNode *root) {
	if (current_function_node == nullptr) return;
	ConsumeChildCode(root);
	auto &o = root->code;
	if (root->dataType.datatype == FLOAT_TYPE) {
		static int f_const_cnt = 0;
		ResolveAddress(root, o, "t2");
		o << "\tlui\tt0,%hi(" << kConstValPrefix << f_const_cnt << ")"
		  << std::endl;
		o << "\tflw\tft0,%lo(" << kConstValPrefix << f_const_cnt << ")(t0)"
		  << std::endl;
		o << "\tfsw\tft0,0(t2)" << std::endl;
		float val =
			float(std::get<double>(std::get<Constant>(root->semantic_value)));
		int val_dst;
		static_assert(sizeof(val) <= sizeof(val_dst));
		memcpy(&val_dst, &val, sizeof(val));
		global_data << ".data\n"
					<< kConstValPrefix << f_const_cnt << ":\t.word\t" << val_dst
					<< "\n.text" << std::endl;
		++f_const_cnt;
	} else if (root->dataType.datatype == INT_TYPE) {
		o << "\tli\tt0, "
		  << std::get<int>(std::get<Constant>(root->semantic_value)) << std::endl;
		ResolveAddress(root, o, "t2");
		o << "\tsw\tt0, 0(t2)" << std::endl;
	} else {
		static int s_const_cnt = 0;
		global_data << ".data\n"
					<< kGlobalStrPrefix << s_const_cnt << ":\t.string\t"
					<< std::get<std::string>(
						   std::get<Constant>(root->semantic_value))
					<< "\n.align 2\n.text" << std::endl;
		root->stack_offset =
			s_const_cnt;  // TODO-CLEAN: Dirty hack to save the label id
		++s_const_cnt;
	}
}

void Generator::ProcessIdentifierNode(AstNode *root, bool is_declare) {
	if (!is_declare &&
		std::get<IdSemanticVal>(root->semantic_value).kind == ARRAY_ID) {
		ResolveArrayDimensions(root);
	}
}

void Generator::ProcessNodeEnd(AstNode *root) {
	auto &o = root->code;
	if (root->nodeType == PROGRAM_NODE) {
		o << global_data.str();
		global_data.str("");
	}
	// o << "# ProcessNodeEnd Line " << root->line_number << std::endl;
	ConsumeChildCode(root);
}

void Generator::ProcessVariableDeclListBegin(AstNode *root) { (void)root; }
void Generator::ProcessVariableDeclListEnd(AstNode *root) { (void)root; }
void Generator::ProcessNonEmptyRelopExprListBegin(AstNode *root) { (void)root; }
void Generator::ProcessNonEmptyRelopExprListEnd(AstNode *root) { (void)root; }
void Generator::ProcessNonEmptyAssignExprListBegin(AstNode *root) { (void)root; }
void Generator::ProcessNonEmptyAssignExprListEnd(AstNode *root) { (void)root; }
void Generator::ProcessStatementListBegin(AstNode *root) { (void)root; }
void Generator::ProcessStatementListEnd(AstNode *root) { (void)root; }
void Generator::ProcessTypeDeclarationBegin(AstNode *root) { (void)root; }
void Generator::ProcessTypeDeclarationEnd(AstNode *root) { (void)root; }

void Generator::ResolveDimAddress(AstNode *now, std::stringstream &ss,
					   std::string dest) {
	assert(now->dim_tmp_offset != 0);
	ss << "\tli\t" << dest << "," << now->dim_tmp_offset << std::endl;
	ss << "\tadd\t" << dest << ",s0," << dest << std::endl;
}

void Generator::ResolveArrayDimensions(AstNode *root) {
	auto &o = root->code;
	std::string name =
		std::get<IdSemanticVal>(root->semantic_value).identifierName;
	ScalarArrayType &type = root->dataType;
	AstNode *dim = root->child;
	std::vector<AstNode *> dims;
	while (dim) {
		dims.push_back(dim);
		dim = dim->rightSibling;
	}

	o << dims[0]->code.str();
	dims[0]->code.str("");
	ResolveAddress(dims[0], o, "t6");
	o << "\tlw\tt5,0(t6)" << std::endl;
	ResolveDimAddress(root, o, "t6");
	o << "\tsw\tt5,0(t6)" << std::endl;
	for (auto i = 1u; i < dims.size(); ++i) {
		o << dims[i]->code.str();
		dims[i]->code.str("");
		ResolveAddress(dims[i], o, "t6");
		o << "\tlw\tt5,0(t6)" << std::endl;
		o << "\tsext.w\tt5,t5" << std::endl;
		ResolveDimAddress(root, o, "t6");
		o << "\tlw\tt4,0(t6)" << std::endl;
		o << "\tsext.w\tt4,t4" << std::endl;
		o << "\tli\tt3," << type.dims[i] << std::endl;
		o << "\tmul\tt4,t4,t3" << std::endl;
		o << "\tadd\tt4,t4,t5" << std::endl;
		o << "\tsw\tt4,0(t6)" << std::endl;
	}
	if (dims.size() != type.dims.size()) {
		for (auto i = dims.size(); i < type.dims.size(); ++i) {
			o << "\tlw\tt4,0(t6)" << std::endl;
			o << "\tsext.w\tt4,t4" << std::endl;
			o << "\tli\tt3," << type.dims[i] << std::endl;
			o << "\tmul\tt4,t4,t3" << std::endl;
			o << "\tsw\tt4,0(t6)" << std::endl;
		}
	}
	o << "\tlw\tt5,0(t6)" << std::endl;
	o << "\tsext.w\tt5,t5" << std::endl;
	o << "\tslli\tt5,t5,2" << std::endl;
	o << "\tsw\tt5,0(t6)" << std::endl;
}

void Generator::ResolveAddress(AstNode *now, std::stringstream &ss, std::string dest) {
	bool is_array =
		std::holds_alternative<IdSemanticVal>(now->semantic_value) &&
		std::get<IdSemanticVal>(now->semantic_value).kind == ARRAY_ID;
	if (now->is_global) {
		std::string name =
			std::get<IdSemanticVal>(now->semantic_value).identifierName;
		ss << "\tlui\t" << dest << ",%hi(" << kGlobalVarPrefix << name << ")"
		   << std::endl;
		ss << "\taddi\t" << dest << "," << dest << ",%lo(" << kGlobalVarPrefix
		   << name << ")" << std::endl;
	} else if (now->is_param && is_array) {
		ss << "\tli\t" << dest << "," << now->stack_offset << std::endl;
		ss << "\tadd\t" << dest << ",s0," << dest << std::endl;
		ss << "\tld\t" << dest << ", "
		   << "0(" << dest << ")" << std::endl;
	} else {
		ss << "\tli\t" << dest << "," << now->stack_offset << std::endl;
		ss << "\tadd\t" << dest << ",s0," << dest << std::endl;
	}
	if (is_array) {
		ResolveDimAddress(now, ss, "t5");
		ss << "\tlw\tt5,0(t5)" << std::endl;
		ss << "\tadd\t" << dest << "," << dest << ",t5" << std::endl;
	}
}

void Generator::ProcessTestNode(AstNode *root, std::stringstream &ss, std::string dest) {
	ResolveAddress(root, ss, "t2");
	if (root->dataType.datatype == INT_TYPE) {
		ss << "\tlw\tt0,0(t2)" << std::endl;
		ss << "\tsext.w\tt0,t0" << std::endl;
		ss << "\tbeqz\tt0," << dest << std::endl;
	} else {
		ss << "\tflw\tft0,0(t2)" << std::endl;
		ss << "\tfmv.s.x\tft1,zero" << std::endl;
		ss << "\tfeq.s\tt0,ft0,ft1" << std::endl;
		ss << "\tbnez\tt0," << dest << std::endl;
	}
}

void Generator::AssignImpl(AstNode *dst, AstNode *src, std::stringstream &o) {
	ResolveAddress(src, o, "t2");
	if (src->dataType.datatype == FLOAT_TYPE) {
		o << "\tflw\tft0,0(t2)" << std::endl;
		ResolveAddress(dst, o, "t2");
		if (dst->dataType.datatype == FLOAT_TYPE) {
			o << "\tfsw\tft0,0(t2)" << std::endl;
		} else {
			o << "\tfcvt.w.s\tt0, ft0, rtz" << std::endl;
			o << "\tsw\tt0,0(t2)" << std::endl;
		}
	} else {
		o << "\tlw\tt0,0(t2)" << std::endl;
		o << "\tsext.w\tt0, t0" << std::endl;
		ResolveAddress(dst, o, "t2");
		if (dst->dataType.datatype == FLOAT_TYPE) {
			o << "\tfcvt.s.w\tft0, t0" << std::endl;
			o << "\tfsw\tft0,0(t2)" << std::endl;
		} else {
			o << "\tsw\tt0,0(t2)" << std::endl;
		}
	}
}

void Generator::ConsumeChildCode(AstNode *root) {
	auto &o = root->code;
	auto *ch = root->child;
	while (ch) {
		o << ch->code.str();
		ch->code.str("");
		ch = ch->rightSibling;
	}
}

void Generator::GenerateAddi(std::stringstream &ss, std::string rd, std::string ra,
				  int imm) {
	if (-2048 > imm || imm > 2047) {
		ss << "\tli\tt6," << imm << std::endl;
		ss << "\tadd\t" << rd << "," << ra << ",t6" << std::endl;
	} else {
		ss << "\taddi\t" << rd << "," << ra << "," << imm << std::endl;
	}
}

void Generator::GenerateSd(std::stringstream &ss, std::string rd, int imm,
				std::string ba) {
	if (-2048 > imm || imm > 2047) {
		ss << "\tli\tt6," << imm << std::endl;
		ss << "\tadd\tt6," << ba << ",t6" << std::endl;
		ss << "\tsd\t" << rd << ",0(t6)" << std::endl;
	} else {
		ss << "\tsd\t" << rd << "," << imm << "(" << ba << ")" << std::endl;
	}
}

void Generator::GenerateLd(std::stringstream &ss, std::string rd, int imm,
				std::string ba) {
	if (-2048 > imm || imm > 2047) {
		ss << "\tli\tt6," << imm << std::endl;
		ss << "\tadd\tt6," << ba << ",t6" << std::endl;
		ss << "\tld\t" << rd << ",0(t6)" << std::endl;
	} else {
		ss << "\tld\t" << rd << "," << imm << "(" << ba << ")" << std::endl;
	}
}

void Generator::GenerateSw(std::stringstream &ss, std::string rd, int imm,
				std::string ba) {
	if (-2048 > imm || imm > 2047) {
		ss << "\tli\tt6," << imm << std::endl;
		ss << "\tadd\tt6," << ba << ",t6" << std::endl;
		ss << "\tsw\t" << rd << ",0(t6)" << std::endl;
	} else {
		ss << "\tsw\t" << rd << "," << imm << "(" << ba << ")" << std::endl;
	}
}

void Generator::GenerateFsw(std::stringstream &ss, std::string rd, int imm,
				std::string ba) {
	if (-2048 > imm || imm > 2047) {
		ss << "\tli\tt6," << imm << std::endl;
		ss << "\tadd\tt6," << ba << ",t6" << std::endl;
		ss << "\tfsw\t" << rd << ",0(t6)" << std::endl;
	} else {
		ss << "\tfsw\t" << rd << "," << imm << "(" << ba << ")" << std::endl;
	}
}
